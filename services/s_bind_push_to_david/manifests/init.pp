class s_bind_push_to_david {
   file { "/etc/bind/push-to-david":
     content => template("s_bind_push_to_david/push.erb"),
     mode    => '0755',
  }
}
