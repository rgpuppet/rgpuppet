class s_dc::etchosts {
  exec { "/bin/echo 192.168.10.105 dc1.ad.ryttargardskyrkan.se dc1 >> /etc/hosts":
    unless => "/bin/grep -q dc1.ad /etc/hosts",
  }
}
