class s_workstation {
  include s_cups
  include s_home
  include s_development
  include git::gui
  include s_swedish
  include ubuntu_desktop
  include thunderbird
  include firefox
  include openoffice
  include flash
  include mp3
  include gv
  include gimp
  include pdf
  include telnet
  include ftp

  # Add the font Caviar Dreams, that is used for the Ryttargardskyrkan logo.
  include caviar_dreams

  file { "/etc/alternatives/x-window-manager":
    ensure => "/usr/bin/metacity",
  }

  # To be able to convert .vcf to .ldif.
  include kaddressbook

  # Ingrid uses korganizer.
  include korganizer
}
