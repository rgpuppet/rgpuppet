class s_home {
  include nfs::client
  include s_nis
  include automount

  file { "/etc/auto.master":
    content => "/home /etc/auto.home\n",
    notify  => Service["autofs"],
  }

  file { "/etc/auto.home":
    content => "* homes.ryttargarden:/home/&\n",
    notify  => Service["autofs"],
  }
}
