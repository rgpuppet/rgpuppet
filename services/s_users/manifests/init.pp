class s_users::opt_home {
  file { "/opt/home":
    ensure => directory,
    owner  => root,
    mode   => '0755',
    tag    => "sysadm",
  }
}

class s_users::admin_group {
  exec { "addgroup admin":
    command => "/usr/sbin/addgroup --system admin",
    unless  => "/bin/grep -q '^admin:' /etc/group",
    tag	   => "sysadm",
  }
}

class s_users::rgxpadmin_group {
  exec { "addgroup rgxpadmin":
    command => "/usr/sbin/addgroup --system rgxpadmin",
    unless  => "/bin/grep -q '^rgxpadmin:' /etc/group",
    tag	   => "sysadm",
  }
}

# Note: all users created by this define must have a (possibly empty)
# entry in the sshgroups setting in hieradata/common.yaml.
define s_users::localuser($fullname, $opt, $local) {
  include s_users::admin_group

  $pwdname = $title
  $uname = $local ? {
     true => "${title}local",
     default => $title,
  }

  $homedir = $opt ? {
    true    => "/opt/home/$uname",
    default => "/home/$uname",
  }

  if $opt {
    include s_users::opt_home
  }

  if $::hostname == "gershom" {
    include s_users::rgxpadmin_group
  }

  host::impl::install_authorized_key_group { "$uname-$pwdname":
    user  => $uname,
    group => $pwdname,
  }

  exec { "adduser $uname":
    command => "/usr/sbin/adduser --home $homedir --gecos \"$fullname\" --disabled-password $uname",
    unless  => "/bin/grep -q '^$uname:' /etc/passwd",
    tag     => "sysadm",
  }

  exec { "addgroup $uname admin":
    command => "/usr/sbin/addgroup $uname admin",
    unless  => "/bin/grep -q '^admin:.*[:,]$uname\\(,\\|\$\\)' /etc/group",
    require => Exec["adduser $uname", "addgroup admin"],
    tag     => "sysadm",
  }

  exec { "addgroup $uname rgxpadmin":
    command => "/usr/sbin/addgroup $uname rgxpadmin",
    unless => "/bin/grep -q '^rgxpadmin:.*[:,]$uname\\(,\\|\$\\)' /etc/group",
    require => Exec["adduser $uname"],
    onlyif => "/bin/grep -q '^rgxpadmin:' /etc/group",
    tag => "sysadm",
  }
  
  exec { "addgroup $uname libvirtd":
    command => "/usr/sbin/addgroup $uname libvirtd",
    unless  => "/bin/grep -q '^libvirtd:.*[:,]$uname\\(,\\|\$\\)' /etc/group",
    onlyif  => "/bin/grep -q '^libvirtd:' /etc/group",
    require => Exec["adduser $uname"],
    tag	   => "sysadm",
  }

  exec { "addgroup ${uname} lpadmin":
    command => "/usr/sbin/addgroup ${uname} lpadmin",
    unless  => "/bin/grep -q '^lpadmin:.*[:,]${uname}\\(,\\|\$\\)' /etc/group",
    onlyif  => "/bin/grep -q '^lpadmin:' /etc/group",
    require => Exec["adduser ${uname}"],
    tag	   => "sysadm",
  }

  $encrypted = generate("/usr/bin/awk",
  	                "-F:",
  	                "\$1 == name && NF == 3 && \$3 == hostname {
			     host = \$2
                         }
   	       		 \$1 == name && NF == 2 { generic = \$2 }
			 END { if (host)
			         p = host;
			       else
			         p = generic;
			       printf(\"%s\", p); }",
			"name=$pwdname",
			"hostname=$hostname",
			"/etc/passwords")

  exec { "password $uname":
    command => "/usr/bin/awk -F: '\$1 == user { \$2 = encrypted; } { OFS=\":\"; print; }' user=$uname encrypted='$encrypted' /etc/shadow > /etc/shadow.next && /bin/cat /etc/shadow.next > /etc/shadow; rm /etc/shadow.next ",
    require => Exec["adduser $uname"],
    unless  => "/bin/grep -F '$uname:$encrypted:' /etc/shadow",
    tag     => "sysadm",
  }
}

define s_users::do_sysadmins($opt, $local) {
  s_users::localuser { "emil":
    fullname => "Emil B\u00E4ckmark",
    opt      => $opt,
    local    => $local,
    tag      => "sysadm",
  }
  s_users::localuser { "andvin":
    fullname => "Andreas Vinsander",
    opt      => $opt,
    local    => $local,
    tag      => "sysadm",
  }
  s_users::localuser { "ceder":
    fullname => "Per Cederqvist",
    opt      => $opt,
    local    => $local,
    tag      => "sysadm",
  }
}

class s_users::sysadmins_local_opt {
  s_users::do_sysadmins { "x":
    opt   => true,
    local => true,
  }
}

class s_users::sysadmins {
  s_users::do_sysadmins { "x":
    opt   => false,
    local => false,
  }
}

class s_users::sysadmins_local {
  s_users::do_sysadmins { "x":
    opt   => false,
    local => true,
  }
}

class s_users::firewall_users {
  include s_users::www_users

  s_users::localuser { "samfors":
    fullname => "Sam Forsberg",
    opt      => false,
    local    => false,
  }

  s_users::localuser { "ingrid":
    fullname => "Ingrid Andersson",
    opt      => false,
    local    => false,
  }
}

class s_users::www_users {
  s_users::localuser { "mathz":
    fullname => "Mats Ahlbom",
    opt      => false,
    local    => false,
  }
}
