class s_ca {
  include easy_rsa

  file { '/etc/openvpn':
    ensure => directory,
  }

  file { '/etc/openvpn/easy-rsa':
    ensure => directory,
  }

  exec { '/bin/cp -r /usr/share/easy-rsa/* /etc/openvpn/easy-rsa/':
    require => File['/etc/openvpn/easy-rsa'],
    creates => '/etc/openvpn/easy-rsa/build-ca',
    notify  => File['/etc/openvpn/easy-rsa/vars.rg'],
  }

  exec { '/bin/echo ". vars.rg" >> /etc/openvpn/easy-rsa/vars':
    unless => '/bin/grep -q vars.rg /etc/openvpn/easy-rsa/vars',
    onlyif => '/usr/bin/test -f /etc/openvpn/easy-rsa/vars',
  }

  file { '/etc/openvpn/easy-rsa/vars.rg':
    content => template('s_ca/vars.erb'),
  }
}
