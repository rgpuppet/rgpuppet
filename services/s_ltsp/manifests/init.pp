class s_ltsp {
  include ltsp_server_standalone
  include ssh::server

  debmirror::sourceslst { "/opt/ltsp/i386/etc/apt/sources.list":
    fallbackmirror => "http://se.archive.ubuntu.com/ubuntu/",
    require        => Package["ltsp-server-standalone"],
  }

  include nagios::nrpe

  @@nagios_service { "$hostname dbus":
    use                 => "nrpe-service",
    host_name           => "$hostname",
    service_description => "dbus",
    check_command       => "check_nrpe_1arg!check_dbus",
    target              => rg_nagios_target("dbus"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  file { "/etc/nagios/nrpe.d/check_dbus.cfg":
    content => "command[check_dbus]=/usr/lib/nagios/plugins/check_file_age -w 2147483647 -c 2147483647 -f /var/run/dbus/system_bus_socket\n",
    notify  => Service["nagios-nrpe-server"],
  }

  file { "/etc/default/locale":
    content => "LANG=\"sv_SE.UTF-8\"\n",
  }

  if versioncmp($lsbdistrelease, "10.04") <= 0 {
    $cfg_console_setup = "/opt/ltsp/i386/etc/default/console-setup"
    exec { "/bin/sed -i 's/XKBVARIANT=\".*\"/XKBVARIANT=\"\"/' $cfg_console_setup":
      unless => "/bin/grep -q 'XKBVARIANT=\"\"' $cfg_console_setup",
      notify => Exec["ltsp-update-image"],
    }
  }
  exec { "ltsp-update-image":
    command => "/usr/sbin/ltsp-update-image -a i386",
    refreshonly => true,
  }
}

class s_ltsp::ltsconf {
  file { "/var/lib/tftpboot/ltsp/i386":
    ensure => "directory",
    owner  => "root",
  }
  file { "/var/lib/tftpboot/ltsp/i386/lts.conf":
    content => template("s_ltsp/lts.conf.erb"),
  }
}
