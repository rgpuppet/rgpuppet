class s_rgxp_server {
  include nfs::client
  include s_nis
  include s_cups
  include automount
  include samba

  Ssh_authorized_key <<| title == "adduser @ sheva" |>>

  file { '/etc/auto.master':
    content => template('s_rgxp_server/auto.master.erb'),
  }

  file { '/etc/auto.samba':
    content => template('s_rgxp_server/auto.samba.erb'),
  }

  file { '/etc/auto.home':
    content => template('s_rgxp_server/auto.home.erb'),
  }

  file { '/etc/samba/smb.conf':
    content => template('s_rgxp_server/smb.conf.erb'),
  }
}
