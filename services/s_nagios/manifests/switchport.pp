define s_nagios::switchport($switch, $switch_port, $hostname,
                            $use_store_config, $contact_groups) {
  s_nagios::switchport_base { $title:
    switch           => $switch,
    switch_port      => $switch_port,
    hostname         => $hostname,
    targetname       => "switchport",
    service_desc     => "switchport",
    use_store_config => $use_store_config,
    contact_groups   => $contact_groups,
  }
}
