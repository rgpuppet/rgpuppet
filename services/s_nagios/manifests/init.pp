# -*- coding: utf-8 -*-
class s_nagios {
  include ntpd::nagios
  include hddtemp::nagios
  include rwho::nagios
  include apache::nagios
  include nagios::check_ifstatus
  include s_nagios::nonpuppet_hosts
  include s_nagios::servicegroups
  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    include s_nagios::nagiosgrapher
  }
  if versioncmp($::lsbdistrelease, '14.04') >= 0 {
    package { 'snmp-mibs-downloader':
      ensure => installed,
    }
  }
  include ups::nagios
  include nis::nagios
  include kvm::nagios
  include s_nullclient::nagios
  include s_squid::nagios
  include nut::nagios
  include puppet::nagios
  include nagios::server
  include aptcacher::nagios
  include s_router::nagios
  include consizone

  file { "/etc/nagios3/conf.d/rg-david-checkcommands.cfg":
    content => template("s_nagios/checkcommands.cfg"),
    notify  => Service["nagios3"],
  }

  file { "/etc/nagios3/conf.d/rg-david-contactgroups.cfg":
    content => template("s_nagios/contactgroups.cfg"),
    notify  => Service["nagios3"],
  }

  file { "/etc/nagios3/conf.d/rg-david-contacts.cfg":
    content => template("s_nagios/contacts.cfg"),
    notify  => Service["nagios3"],
  }

  file { "/etc/nagios3/conf.d/rg-david-hostgroups.cfg":
    content => template("s_nagios/hostgroups.cfg"),
    notify  => Service["nagios3"],
  }

  file { "/etc/nagios3/conf.d/rg-david-hosts.cfg":
    content => template("s_nagios/hosts.cfg"),
    notify  => Service["nagios3"],
  }

  file { "/etc/nagios3/conf.d/rg-david-services.cfg":
    content => template("s_nagios/services.cfg"),
    notify  => Service["nagios3"],
  }
}

define s_nagios::bare_switchport($switch, $switch_port, $contact_groups) {
  $host = $title
  s_nagios::switchport_base { "$switch switchport $switch_port":
    switch           => "$switch",
    switch_port      => "$switch_port",
    hostname         => "$switch",
    targetname       => "switchport-$switch",
    service_desc     => "switchport $host",
    use_store_config => false,
    contact_groups   => $contact_groups,
  }
}

define s_nagios::nonpuppet_host($hostgroup, $comment="", $host_use="rg-host",
       			        $switch=false, $switch_port=false,
				$parent=undef, $address=false,
                                $contact_groups=undef) {
  $hostname = $title

  $commenttrail = $comment ? {
    "" => "",
    default => " - $comment"
  }


  $lookupresult = $address ? {
    false   => split( generate( "/usr/bin/getent", "--service=dns",
    	       	      		"hosts", "$hostname" ), ' '),
    default => $address
  }

  if $contact_groups {
    $real_contact_groups = $contact_groups
  }
  else {
    $real_contact_groups = hiera('nagios::contact_groups')
  }

  nagios_host { "$hostname":
    use            => "$host_use",
    alias          => "$hostname$commenttrail",
    address        => $lookupresult,
    parents        => $parent,
    hostgroups     => $hostgroup,
    contact_groups => $real_contact_groups,
  }

  nagios_service { "$hostname ping":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "PING",
    check_command       => "check_ping!200.0,20%!500.0,60%",
    target              => rg_nagios_target("ping"),
    contact_groups      => $real_contact_groups,
  }

  if $switch {
    s_nagios::switchport { "$switch switchport $switch_port":
      switch           => $switch,
      switch_port      => $switch_port,
      hostname         => $hostname,
      use_store_config => false,
      contact_groups   => $real_contact_groups,
    }
  }
}

define s_nagios::non_ip_host($switch, $switch_port, $hostgroup,
                             $parent, $contact_groups,
                             $comment="", $host_use="rg-host") {
  $hostname = $title

  $commenttrail = $comment ? {
    "" => "",
    default => " - $comment"
  }

  nagios_host { "$hostname":
    use            => "$host_use",
    alias          => "$hostname$commenttrail",
    address        => "127.0.0.1",
    parents        => $parent,
    check_interval => "1.0",
    retry_interval => "1.0",
    check_command  => "check-snmp-ifstatus-link!$switch!$switch_port",
    hostgroups     => $hostgroup,
    contact_groups => $contact_groups,
  }

  s_nagios::switchport { "$switch switchport $switch_port":
    switch           => $switch,
    switch_port      => $switch_port,
    hostname         => $hostname,
    use_store_config => false,
    contact_groups   => $contact_groups,
  }
}

class s_nagios::nonpuppet_hosts {
  s_nagios::nonpuppet_host { "switch":
    comment   => "main switch",
    parent    => "switch-1104",
    hostgroup => "network",
  }

  s_nagios::nonpuppet_host { "wireless4":
    comment     => "WiFi i kyrksalen",
    parent      => "switch-01-04b",
    switch      => "switch-01-04b",
    switch_port => 6,
    hostgroup   => "network",
  }

  s_nagios::nonpuppet_host { 'wireless5':
    comment     => 'WiFi in the office of fritids',
    parent      => 'switch-01-04b',
    switch      => 'switch-01-04b',
    switch_port => 12,
    hostgroup   => 'network',
  }

  s_nagios::nonpuppet_host { 'wireless6':
    comment     => 'WiFi outside office',
    parent      => 'switch-1410',
    switch      => 'switch-1410',
    switch_port => 6,
    hostgroup   => 'network',
  }

  s_nagios::nonpuppet_host { "laser":
    comment     => "printer",
    switch      => "switch",
    switch_port => 12,
    host_use    => "rg-printer",
    parent      => "switch",
    hostgroup   => "printers",
  }

  s_nagios::nonpuppet_host { "kyocera4550":
    comment     => "printer",
    switch      => "switch-1410",
    switch_port => "2",
    host_use    => "rg-printer",
    parent      => "switch-1410",
    hostgroup   => "printers",
  }

  s_nagios::nonpuppet_host { "pcmurre":
    comment     => "Ekonomi-PC (Windows XP)",
    switch      => "switch-1410",
    switch_port => "1",
    parent      => "switch-1410",
    hostgroup   => "winxp",
  }

  s_nagios::nonpuppet_host { "pcmaja":
    comment     => "Monica Johansson (nya kontorsrummet)",
    switch      => "switch",
    switch_port => "9",
    parent      => "switch",
    hostgroup   => "winxp",
  }

  s_nagios::nonpuppet_host { "pcolle":
    comment     => "Monica Karlsson (thin client)",
    switch      => "switch",
    switch_port => 24,
    parent      => "switch",
    hostgroup   => "thin",
  }

  s_nagios::nonpuppet_host { "pcrickard":
    comment     => "Fredrik Lignell (thin client)",
    switch      => "switch",
    switch_port => 6,
    parent      => "switch",
    hostgroup   => "thin",
  }

  s_nagios::nonpuppet_host { "pchilda":
    comment     => "Gun och Britt-Marie (thin client)",
    switch      => "switch",
    switch_port => "3",
    parent      => "switch",
    hostgroup   => "thin",
  }

  s_nagios::nonpuppet_host { "pckalle":
    comment     => "Ingrid (thin client)",
    parent      => 'snom-expedition',
    hostgroup   => "thin",
  }

  s_nagios::nonpuppet_host { "pclaban":
    comment     => "Ekonomi (Windows XP)",
    switch      => "switch",
    switch_port => 11,
    parent      => "switch",
    hostgroup   => "winxp",
  }

  s_nagios::nonpuppet_host { "pcville":
    comment     => "Camilla Sundberg (thin client)",
    switch      => "switch",
    switch_port => "8",
    parent      => "switch",
    hostgroup   => "thin",
  }

  s_nagios::bare_switchport { "kollektomat":
    switch         => 'switch',
    switch_port    => 21,
    contact_groups => 'linux-admins',
  }

  s_nagios::bare_switchport { "sunny-webbox":
    switch         => 'switch',
    switch_port    => 15,
    contact_groups => 'linux-admins',
  }

  s_nagios::non_ip_host { "ADSL":
    comment        => 'ADSL modem',
    switch         => 'switch',
    switch_port    => 22,
    parent         => 'switch',
    hostgroup      => 'network',
    contact_groups => 'linux-admins',
  }

  s_nagios::non_ip_host { "fiber":
    comment        => 'Fiber converter',
    switch         => 'switch-01-04b',
    switch_port    => 21,
    parent         => 'switch-01-04b',
    hostgroup      => 'network',
    contact_groups => 'linux-admins',
  }

  s_nagios::nonpuppet_host { "samuel":
    comment     => "Misc server on DMZ",
    switch      => "switch-1104",
    switch_port => "4",
    host_use    => "dmz-host",
    parent      => "switch-1104",
    hostgroup   => "servers",
  }

  s_nagios::nonpuppet_host { "switch-1410":
    comment     => "switch in office",
    switch      => "switch-01-04b",
    switch_port => 24,
    parent      => "switch-01-04b",
    hostgroup   => "network",
  }

  s_nagios::bare_switchport { "uplink":
    switch         => 'switch-1410',
    switch_port    => '8',
    contact_groups => 'linux-admins',
  }

  s_nagios::nonpuppet_host { "switch-1104":
    comment     => "switch in basement",
    switch      => "switch",
    switch_port => 25,
    parent      => 'elia',
    hostgroup   => "network",
  }

  s_nagios::nonpuppet_host { "switch-01-04b":
    comment     => "Gbit-switch in rack in the office",
    parent      => "switch",
    hostgroup   => "network",
    switch      => "switch",
    switch_port => 26,
  }

  s_nagios::nonpuppet_host { 'switch-netgear':
    comment     => 'switch in basement',
    switch      => 'switch-01-04b',
    switch_port => 22,
    parent      => 'switch-01-04b',
    hostgroup   => 'network',
  }

  s_nagios::nonpuppet_host { "camera-1":
    comment        => 'kamera i kyrksalen',
    parent         => 'wireless4',
    hostgroup      => 'alarm',
    contact_groups => 'surveillance-admins',
  }

  s_nagios::nonpuppet_host { "lukas":
    comment     => "camera server in basement",
    parent      => "switch-1104",
    switch      => "switch-1104",
    switch_port => "3",
    hostgroup   => "alarm",
  }

  s_nagios::nonpuppet_host { 'wifi-bridge-cafe':
    comment     => 'TP-LINK near cafe',
    parent      => 'wireless6',
    hostgroup   => 'network',
  }

  s_nagios::nonpuppet_host { 'gigaset-dect-bridge':
    comment     => 'Gigaset N510 IP PRO DECT bridge',
    parent      => 'switch',
    switch      => 'switch',
    switch_port => 16,
    hostgroup   => 'phones',
  }

  s_nagios::nonpuppet_host { 'snom-expedition':
    comment     => 'snom 760 IP Phone',
    parent      => 'switch',
    switch      => 'switch',
    switch_port => 10,
    hostgroup   => 'phones',
  }

  s_nagios::nonpuppet_host { 'snom-expedition-2':
    comment     => 'snom 760 IP Phone',
    parent      => 'switch-1410',
    switch      => 'switch-1410',
    switch_port => 3,
    hostgroup   => 'phones',
  }

  s_nagios::nonpuppet_host { 'snom-cafe':
    comment     => 'SNOM 320 in the cafe',
    parent      => 'wifi-bridge-cafe',
    hostgroup   => 'phones',
  }
}

class s_nagios::servicegroups {
  nagios_servicegroup { "switchport":
    alias => "switchport",
  }
}

class s_nagios::nagiosgrapher {
  file { "/etc/nagiosgrapher/ngraph.d/switchport.ncfg":
    content => template("s_nagios/switchport.ncfg.erb"),
    notify  => Service["nagiosgrapher"],
  }
}

class s_nagios::fix_nagios_user {
  exec { "/usr/bin/ypmatch nagios passwd >> /etc/passwd":
    onlyif => "/usr/bin/test -f /usr/bin/ypmatch && /usr/bin/ypmatch nagios passwd>/dev/null",
    unless => "/bin/grep '^nagios:' /etc/passwd",
    notify => Service["nagios-nrpe-server"],
  }
  exec { "/bin/echo 'nagios:*:15018:0:99999:7:::' >> /etc/shadow":
    onlyif => "/usr/bin/test -f /usr/bin/ypmatch && /usr/bin/ypmatch nagios passwd>/dev/null",
    unless => "/bin/grep '^nagios:' /etc/shadow",
    notify => Service["nagios-nrpe-server"],
  }
  exec { "/usr/bin/ypmatch nagios group >> /etc/group":
    onlyif => "/usr/bin/test -f /usr/bin/ypmatch && /usr/bin/ypmatch nagios group>/dev/null",
    unless => "/bin/grep '^nagios:' /etc/group",
    notify => Service["nagios-nrpe-server"],
  }
}
