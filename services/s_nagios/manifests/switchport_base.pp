define s_nagios::switchport_base($switch, $switch_port, $service_desc,
       				 $hostname, $targetname, $use_store_config,
                                 $contact_groups) {

  $need_32bit = $switch ? {
    'switch-1104'    => true,
    'switch-1410'    => true,
    'switch-netgear' => true,
    default          => false,
  }

  if $need_32bit {

    # Ports known to run 100 Mbps or less can safely be sampled once
    # per minute.  Other ports must be sampled more often; we use 5
    # times per minute to have some spare headroom.
    $interval = "${switch}-${switch_port}" ? {
      'switch-1410-1' => '1.0',
      'switch-1410-6' => '1.0',
      default         => '0.2',
    }

    $service = 'check-snmp-ifstatus-32bit'
  }
  else {
    $interval = '1.0'
    $service = 'check-snmp-ifstatus'
  }

  $service_desc_safe = regsubst($service_desc, ' ', '', 'G')

  if $use_store_config {
    @@nagios_service { "$title":
      use                 => "rg-service",
      host_name           => "$hostname",
      service_description => "$service_desc",
      servicegroups       => "switchport",
      check_command       => "${service}!${switch}!${switch_port}",
      check_interval      => $interval,
      retry_interval      => $interval,
      target              => rg_nagios_target($targetname),
      contact_groups      => $contact_groups,
      icon_image          => rg_graphios("${service_desc_safe}.{RXbps,TXbps}"),
    }
  }
  else {
    nagios_service { "$title":
      use                 => "rg-service",
      host_name           => "$hostname",
      service_description => "$service_desc",
      servicegroups       => "switchport",
      check_command       => "${service}!${switch}!${switch_port}",
      check_interval      => $interval,
      retry_interval      => $interval,
      target              => rg_nagios_target($targetname),
      contact_groups      => $contact_groups,
      icon_image          => rg_graphios("${service_desc_safe}.{RXbps,TXbps}"),
    }
  }
}
