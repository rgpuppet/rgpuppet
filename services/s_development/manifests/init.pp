class s_development {
  include vc::mercurial
  include vc::cvs
  include git
  include syslinux
  include build_essential
  include gcc
  include autoconf
  include automake
  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    include automake::v1_7
  }
  include valgrind
  include emacs::exuberant_ctags
  include emacs::puppet
  include emacs::yaml_mode
}
