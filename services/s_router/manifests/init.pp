class s_router($router_interfaces = hiera('s_router::interfaces'),
               $ddclientuser = hiera('s_router::ddclientuser'),
               $ddclientpassword = hiera('s_router::ddclientpassword'),
) {
  include network::common
  include bridge_utils
  include vlan
  include ntpd::static_ip

  $router = $hostname ? {
    "$main_router" => true,
    default        => false,
  }

  # The rwhod of netkit-rwho/0.17-10 does not understand the "-i" option.
  # See  # <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=468491>. 
  $broken_rwhod = $lsbdistrelease ? {
    "8.04"  => true,
    "8.10"  => true,
    default => false,
  }

  file { "/etc/init.d/firewall":
    content => template("s_router/firewall.erb"),
    notify  => Service["firewall"],
    mode    => '0755',
  }

  file { "/etc/network/interfaces":
    content => template("s_router/interfaces.erb"),
    owner   => root,
    group   => root,
    mode    => '0644',
    notify  => [Service["networking"], Service["firewall"]],
  }

  if versioncmp($::lsbdistrelease, '12.04') < 0 {
    $dhclientdir = '/etc/dhcp3'
  }
  else {
    $dhclientdir = '/etc/dhcp'
  }

  file { "${dhclientdir}/dhclient.conf":
    content => template("s_router/dhclient.conf.erb"),
    notify  => [Service["networking"], Service["firewall"]],
  }

  file { "${dhclientdir}/dhclient-exit-hooks.d/rg-forwarding-setup":
    content => template("s_router/forwarding.erb"),
    notify  => Service["networking"],
    mode    => '0755',
  }

  service { "firewall":
    enable  => true,
  }

  case $broken_rwhod {
    true: {
      # The -i option is broken in 8.04, so we fallback to
      # broadcasting to all interfaces, and let the firewalling code
      # stop the broadcast to the world.
      rwho::options { "-b": }
    }
    false: {
      rwho::options { "-i br0 -i br1": }
    }
  }

  $check_router = "/usr/lib/nagios/plugins/check_router"
  $check_router_cfg = "/etc/nagios/nrpe.d/router.cfg"

  if $router {
    package { 'ddclient':
      ensure => present,
    }

    file { '/etc/ddclient.conf':
      require => Package['ddclient'],
      notify  => Service['ddclient'],
      content => template('s_router/ddclient.conf.erb'),
    }

    service { 'ddclient':
      ensure => running,
      enable => true,
    }

    file { "$check_router":
      content => template("s_router/check_router.erb"),
      mode    => '0755',
    }

    file { "$check_router_cfg":
      content => "command[check_router]=$check_router\n"
    }

    file { '/etc/init.d/setup-shaping':
      content => template('s_router/shaping.erb'),
      mode    => '0555',
    }

    @@nagios_host { "nexthop":
      use            => "external-host",
      alias          => "nexthop router",
      address        => "$ipaddress",
      check_command  => "check_nrpe_1arg!check_router",
      parents        => "ADSL",
      hostgroups     => "external",
      contact_groups => hiera('nagios::contact_groups'),
    }

    @@nagios_service { "nexthop ping":
      use                 => "rg-service",
      host_name           => "nexthop",
      service_description => "ping",
      check_command       => "check_nrpe_1arg!check_router",
      target              => rg_nagios_target("ping-nexthop"),
      contact_groups      => hiera('nagios::contact_groups'),
    }

    @@nagios_service { "router ping":
      use                 => "rg-service",
      host_name           => "$hostname",
      service_description => "ping router ip",
      check_command       => "check-ping-host!$gateway!200.0,20%!500.0,60%",
      target              => rg_nagios_target("ping-router"),
      contact_groups      => hiera('nagios::contact_groups'),
    }
  } else {
    package { 'ddclient':
      ensure => absent,
    }

    file { "$check_router":
      ensure => absent,
    }

    file { "$check_router_cfg":
      ensure => absent,
    }

    file { '/etc/init.d/setup-shaping':
      ensure => absent,
    }
  }
}
