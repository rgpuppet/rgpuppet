class s_router::nagios {
    nagios_command { "check-ping-host":
      command_line => "/usr/lib/nagios/plugins/check_ping -H '\$ARG1\$' -w '\$ARG2\$' -c '\$ARG3\$'",
      notify  => Service["nagios3"],
    }
}
