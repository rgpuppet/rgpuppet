class s_rwho {
  include rwho

  s_rwho::retired { "monitor": }
  s_rwho::retired { "pcgullan": }
  s_rwho::retired { "puppet": }
  s_rwho::retired { "jeos-9-10-ltsp": }
  s_rwho::retired { "ltsp": }
}

define s_rwho::retired {
  $host = $title

  file { "/var/spool/rwho/whod.$host":
    ensure => absent,
  }
}
