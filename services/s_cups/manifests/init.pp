class s_cups {
  include cups
  cups::client { "printspool.ryttargarden": 
  }
}

class s_cups::server {
  include cups

  $conf = '/etc/cups/cupsd.conf'

  exec { "/bin/sed -i 's/^Listen .*:631\$/Listen *:631/' ${conf}":
    unless => "/bin/grep -q '^Listen \\*:631\$' ${conf}",
    notify => Service["cups"],
  }

  exec { "/bin/sed -i 's/^Browsing .*\$/Browsing On/' ${conf}":
    unless => "/bin/grep -q '^Browsing On\$' ${conf}",
    notify => Service["cups"],
  }

  exec { "/bin/echo 'DefaultPaperSize A4' >> ${conf}":
    unless => "/bin/grep -q '^DefaultPaperSize A4\$' ${conf}",
    notify => Service["cups"],
  }

  # Add an "Allow from @LOCAL" to all <Location> sections.
  exec { "/bin/sed -i '/^<Location/a\\\n  Allow from @LOCAL' ${conf}":
    unless => "/bin/grep -q '  Allow from @LOCAL\$' ${conf}",
    notify => Service["cups"],
  }

  exec { "/bin/echo 'ServerAlias *' >> ${conf}":
    unless => "/bin/grep -q '^ServerAlias ' ${conf}",
    notify => Service["cups"],
  }
}
