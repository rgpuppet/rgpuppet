class s_swedish {
  include language_pack_sv
  include language_pack_gnome_sv
  include myspell_dictionary_sv
  include openoffice::sv
  include thunderbird::sv
  if versioncmp($lsbdistrelease, "10.04") <= 0 {
    include gnome_user_guide_sv
    include language_support_sv
  }
}
