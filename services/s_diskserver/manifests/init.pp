class s_diskserver {
  include s_users::sysadmins_local_opt
  include ssh::server
  include puppet
  include rsync
  include ssh::permit_root
  include emacs
  # include s_nis
  include nis

  nis::conf { "ryttargarden.nis":
    server => "192.168.10.138",
  }

  include s_development

  $opts = 'rw,mp,no_root_squash,no_subtree_check,sync'
  $how = "$office_network/$office_mask($opts)"

  nfs::server { "dummy title":
    exports => [ "/home $how",
                 "/samba $how" ],
  }
}
