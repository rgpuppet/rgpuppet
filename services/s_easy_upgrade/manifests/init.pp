class s_easy_upgrade {
  exec { "/bin/echo '%admin ALL=(ALL) NOPASSWD: /usr/bin/apt-get dist-upgrade' >> /etc/sudoers":
    unless => "/bin/grep -q 'admin.*apt-get dist-upgrade' /etc/sudoers",
  }

  exec { "/bin/echo '%admin ALL=(ALL) NOPASSWD: /usr/bin/apt-get update' >> /etc/sudoers":
    unless => "/bin/grep -q 'admin.*apt-get update' /etc/sudoers",
  }

  exec { "/bin/echo '%admin ALL=(ALL) NOPASSWD: /sbin/reboot' >> /etc/sudoers":
    unless => "/bin/grep -q 'admin.*sbin/reboot' /etc/sudoers",
  }

  exec { "/bin/echo '%admin ALL=(ALL) NOPASSWD: /usr/local/bin/list-openvpn-connections' >> /etc/sudoers":
    unless => "/bin/grep -q 'admin.*usr/local/bin/list-openvpn-connections' /etc/sudoers",
  }

  file { "/usr/local/bin/up":
    mode => '0755',
    content => template("s_easy_upgrade/up.erb"),
  }

  file { "/usr/local/bin/reboot":
    mode => '0755',
    content => template("s_easy_upgrade/reboot.erb"),
  }

  file { "/usr/local/bin/rmkernel":
    mode => '0755',
    content => template("s_easy_upgrade/rmkernel.erb"),
  }

  file { "/usr/local/bin/list-openvpn-connections":
    mode => '0755',
    content => template("s_easy_upgrade/list-openvpn-connections.erb"),
  }

}
