class s_denyhosts {
  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    include denyhosts

    $cfg = "/etc/denyhosts.conf"

    $email = "sysadmin@ryttargardskyrkan.se"
    exec { "/bin/sed -i 's/^\\(ADMIN_EMAIL\\) *=.*/\\1=$email/' $cfg":
      unless => "/bin/grep -q '^ADMIN_EMAIL=$email\$' $cfg",
      notify => Service["denyhosts"],
    }

    $from = "DenyHosts <nobody@ryttargardskyrkan.se>"
    exec { "/bin/sed -i 's/^\\(SMTP_FROM\\) *=.*/\\1=$from/' $cfg":
      unless => "/bin/grep -q '^SMTP_FROM=$from\$' $cfg",
      notify => Service["denyhosts"],
    }

    exec { "/bin/sed -i 's/^SMTP_SUBJECT = .*/SMTP_SUBJECT = DenyHosts Report $hostname/' $cfg":
      unless => "/bin/grep -q '^SMTP_SUBJECT = DenyHosts Report $hostname\$' $cfg",
      notify => Service["denyhosts"],
    }

    exec { "/bin/sed -i 's/^\\(#\\|\\)\\(SYSLOG_REPORT=YES\\)/\\2/' $cfg":
      unless => "/bin/grep -q '^SYSLOG_REPORT=YES\$' $cfg",
      notify => Service["denyhosts"],
    }

    exec { "/bin/sed -i 's/^\\(#\\|\\)\\(RESET_ON_SUCCESS = yes\\)/\\2/' $cfg":
      unless => "/bin/grep -q '^RESET_ON_SUCCESS = yes\$' $cfg",
      notify => Service["denyhosts"],
    }
  }
  else {
    include fail2ban

    file { '/etc/fail2ban/fail2ban.local':
      content => template('s_denyhosts/fail2ban.local.erb'),
      notify  => Service['fail2ban'],
    }
  }
}
