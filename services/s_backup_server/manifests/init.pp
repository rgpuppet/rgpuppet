class s_backup_server {
  include s_users::sysadmins_local

  include s_development
  include vc::subversion
  include telnet

  file { "/root/.ssh/config":
     content => "Host samuel\n  HostName 192.168.0.2\n",
  }

  # Also install zlib, Python, librsync and rdiff-backup from source
  # in /opt/LYSrdiff.  See instructions in ~ceder/lysrdiff-src on
  # pcfritz.

  # Also install LYSrdiff:
  #
  # svn co svn+ssh://ceder@lsvn.lysator.liu.se/svnroot/lysrdiff/trunk/lysrdiff
  # cd lysrdiff
  # make install

  # Also install nagios-plugins from Lysator (see comment @ symeon)

  include s_lysrdiff
}
