class s_nullclient(
  $domain=hiera('s_nullclient::domain'),
  $relay=hiera('s_nullclient::relay'),
  $mynetworks=hiera('s_nullclient::mynetworks'),
  $myhostname=hiera('s_nullclient::myhostname'),
  $inet_interfaces=hiera('s_nullclient::inet_interfaces'),
  $rate_delay=hiera('s_nullclient::rate_delay'),
) {
  include s_nullclient::monitor

  email::nullclient { $domain:
    relay           => $relay,
    mynetworks      => $mynetworks,
    myhostname      => $myhostname,
    inet_interfaces => $inet_interfaces,
    rate_delay      => $rate_delay,
  }
}
