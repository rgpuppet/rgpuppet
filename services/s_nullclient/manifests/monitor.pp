class s_nullclient::monitor {
  include nagios::nrpe
  include nagios::plugins_standard

  @@nagios_service { "$hostname mailq":
    use                 => "nrpe-service",
    host_name           => "$hostname",
    service_description => "mailq",
    servicegroups       => "mailq",
    check_command       => "check_nrpe_1arg!check_mailq",
    target              => rg_nagios_target("mailq"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  file { "/etc/nagios/nrpe.d/check_mailq.cfg":
    content => "command[check_mailq]=/usr/lib/nagios/plugins/check_mailq_postfix\n",
    notify  => Service["nagios-nrpe-server"],
  }

  file { '/usr/lib/nagios/plugins/check_mailq_postfix':
    content => template('s_nullclient/check_mailq_postfix.erb'),
    mode    => '0755',
  }

  @@nagios_service { "$hostname smtp localhost":
    use                 => "nrpe-service",
    host_name           => "$hostname",
    service_description => "smtp localhost",
    check_command       => "check_nrpe_1arg!check_smtp_localhost",
    target              => rg_nagios_target("smtp-localhost"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  file { "/etc/nagios/nrpe.d/check_smtp_localhost.cfg":
    content => "command[check_smtp_localhost]=/usr/lib/nagios/plugins/check_smtp -H localhost\n",
    notify  => Service["nagios-nrpe-server"],
  }
}
