class s_squid {
  include squid

  file { "/etc/${squid::squid}/squid.conf":
    content => template("s_squid/${squid::squid}.conf.erb"),
    notify  => Service[$squid::squid],
  }

  @@nagios_service { "${hostname} squid-www":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "HTTP squid www",
    check_command       => "check-squid!http://www.ryttargarden/",
    target              => rg_nagios_target("squid-www"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  @@nagios_service { "${hostname} squid-wiki":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "HTTP squid wiki",
    check_command       => "check-squid!http://wiki.ryttargarden/",
    target              => rg_nagios_target("squid-wiki"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  @@nagios_service { "${hostname} squid-rt":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "HTTP squid rt",
    check_command       => "check-squid!http://rt.ryttargarden/",
    target              => rg_nagios_target("squid-rt"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  @@nagios_service { "${hostname} squid-cups":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "HTTP squid cups",
    check_command       => "check-squid!http://printspool.ryttargarden:631/",
    target              => rg_nagios_target("squid-cups"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  s_squid::snmp { "squid-cpu":
    oid   => "1.3.1.5.0",
    warn  => 50,
    crit  => 70,
    desc  => "Squid CPU usage",
    units => "percent"
  }

  s_squid::snmp { "squid-objs":
    oid   => "1.3.1.7.0",
    desc  => "Squid cached objects",
    units => "objects",
  }

  s_squid::snmp { "squid-fds":
    oid   => "1.3.1.12.0",
    warn  => 200,
    crit  => 240,
    desc  => "Squid file descriptors",
    units => "fds"
  }

  s_squid::snmp { "squid-hitratio":
    oid   => "1.3.2.2.1.9.5",
    desc  => "Squid hit ratio",
    units => "percent",
  }
}

define s_squid::snmp($desc, $oid, $units, $warn=false, $crit=false) {

  if $warn {
    @@nagios_service { "${hostname} ${title}":
      use                 => "rg-service",
      host_name           => "$hostname",
      service_description => "$desc",
      check_command       => "check-squid-snmp!$oid!$warn!$crit",
      target              => rg_nagios_target("$title"),
      contact_groups      => hiera('nagios::contact_groups'),
    }
  } else {
    @@nagios_service { "${hostname} ${title}":
      use                 => "rg-service",
      host_name           => "$hostname",
      service_description => "$desc",
      check_command       => "check-squid-snmp-2!$oid",
      target              => rg_nagios_target("$title"),
      contact_groups      => hiera('nagios::contact_groups'),
    }
  }

  @@file { "/etc/nagiosgrapher/ngraph.d/check_$title.ncfg":
    content => template("s_squid/ngraph.erb"),
    notify  => Service["nagiosgrapher"],
    tag     => "nagiosgrapher",
  }
}

class s_squid::nagios {
  nagios_command { "check-squid":
    command_line => "\$USER1\$/check_http -H \$HOSTADDRESS\$ -p 8080 -u \$ARG1\$ -w 30 -c 80 -t 200",
    notify  => Service["nagios3"],
  }

  nagios_command { "check-squid-snmp":
    command_line => "\$USER1\$/check_snmp -H \$HOSTADDRESS\$ -p 3401 -o .1.3.6.1.4.1.3495.\$ARG1\$ -w \$ARG2\$ -c \$ARG3\$",
    notify  => Service["nagios3"],
  }

  nagios_command { "check-squid-snmp-2":
    command_line => "\$USER1\$/check_snmp -H \$HOSTADDRESS\$ -p 3401 -o .1.3.6.1.4.1.3495.\$ARG1\$",
    notify  => Service["nagios3"],
  }

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    File <<| tag == "nagiosgrapher" |>>
  }
}
