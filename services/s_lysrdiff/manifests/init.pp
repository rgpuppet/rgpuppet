class s_lysrdiff {
  # FIXME: See node pcfritz.  There are a lot more that needs to be done.

  include nagios::nrpe

  s_lysrdiff::nagios { "$hostname": }

  file { "/etc/nagios/nrpe.d/check_lysrdiff.cfg":
    content => "command[check_lysrdiff]=/usr/local/nagios/libexec/check_lysrdiff 1 0\n",
    notify  => Service["nagios-nrpe-server"],
  }
}

define s_lysrdiff::nagios() {
  $hostname = $title
  @@nagios_service { "$hostname check-lysrdiff":
    use                 => "nrpe-service",
    host_name           => "$hostname",
    service_description => "LYSrdiff",
    check_command       => "check_nrpe_1arg!check_lysrdiff",
    target              => rg_nagios_target("lysrdiff"),
    contact_groups      => hiera('nagios::contact_groups'),
    icon_image          => rg_graphios(
      'LYSrdiff.{failed,stale}',
      '&areaMode=all&colorList=red,orange&yMin=0',
      '&bgcolor=black'),
  }
}
