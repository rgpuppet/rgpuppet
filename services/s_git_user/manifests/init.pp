class s_git_user {

  $user = 'git'

  user { $user:
    comment    => 'Git user for puppet repo',
    ensure     => present,
    gid        => $user,
    home       => "/home/${user}",
    managehome => true,
    system     => true,
    shell      => '/bin/bash',
  }

  group { $user:
    ensure => present,
    system => true,
  }
}
