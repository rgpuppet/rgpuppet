class s_dns::master {
  include s_dns

  $zones = $s_dns::zones

  file { "/etc/bind/named.conf.local":
    content => template("s_dns/named.conf.master.erb"),
    notify  => Service["bind9"],
  }

  file { "/etc/bind/named.conf.options":
    content => template("s_dns/named.conf.options.master.erb"),
    notify  => Service["bind9"],
  }
}
