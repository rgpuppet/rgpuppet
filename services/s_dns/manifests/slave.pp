class s_dns::slave {
  include s_dns

  $master = $ns1
  $zones = $s_dns::zones

  file { "/etc/bind/named.conf.local":
    content => template("s_dns/named.conf.slave.erb"),
    notify  => Service["bind9"],
  }

  file { "/etc/bind/named.conf.options":
    content => template("s_dns/named.conf.options.slave.erb"),
    notify  => Service["bind9"],
  }
}
