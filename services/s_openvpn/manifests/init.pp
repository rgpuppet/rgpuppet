class s_openvpn {
  include openvpn
  include ipv4_forward

  file { '/etc/openvpn/server.conf':
    content => template('s_openvpn/server.conf.erb'),
  }

  file { '/etc/init.d/openvpnfw':
    content => template('s_openvpn/openvpnfw.erb'),
    notify  => Service['openvpnfw'],
    mode    => '0755',
  }

  service { 'openvpnfw':
    enable  => true,
  }
}
