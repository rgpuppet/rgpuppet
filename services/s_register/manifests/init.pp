class s_register {
  include churchinfo
#  include s_register::backup

  file { '/var/www/html/phpinfo.php':
    source => 'puppet:///modules/s_register/phpinfo.php',
    owner  => 'root',
    group  => 'root',
  }
  
  # Copy manually from /usr/share/doc/mediawiki/examples/AdminSettings.sample
  # and set the passwords.
  # file { "/etc/churchinfo/AdminSettings.php":
  #   owner => "root",
  #   group => "root",
  #   mode  => '0600',
  # }

#  $site = "/etc/apache2/sites-available/default"

  # exec { "/bin/sed -i 's%\\(.*DocumentRoot\\).*%\\1 /var/www%' $site":
  #   unless => "/bin/grep -q 'DocumentRoot /var/www' $site",
  #   notify => Service["apache2"],
  # }

  # $cfg = "/etc/mediawiki/LocalSettings.php"

  # exec { "/bin/echo \"\\\$wgGroupPermissions['*']['edit'] = false;\" >> $cfg":
  #   unless => "/bin/grep -q '.wgGroupPermissions..*....edit..' $cfg",
  # }

  # exec { "/bin/echo '\$wgLogo = \"/images/logo.jpg\";' >> $cfg":
  #   unless => "/bin/grep -q 'wgLogo = ' $cfg",
  # }

  # $wgServer = "http://wiki.ryttargarden"

  # exec { "/bin/echo '\$wgServer = \"$wgServer\";' >> $cfg":
  #   unless => "/bin/grep -q 'wgServer =' $cfg",
  # }

  # exec { "/bin/sed -i 's%\\(.wgServer = \"\\).*%\\1$wgServer\";%' $cfg":
  #   unless => "/bin/grep -q 'wgServer = \"$wgServer\"' $cfg",
  # }

  # exec { "/bin/sed -i 's/\\(.wgEnableUploads *= *\\).*/\\1 true;/' $cfg":
  #   unless => "/bin/grep -q 'wgEnableUploads *= *true' $cfg",
  # }

#  file { "/usr/share/mediawiki/images/logo.jpg":
#    source => "puppet:///modules/s_wiki/logo.jpg",
#  }
}

# class s_register::backup {
#   include cron

#   $cfg = "/etc/mediawiki/LocalSettings.php"

#   exec { "/bin/echo '\$wgReadOnlyFile = \"/var/run/mediawiki.lock\";' >> $cfg":
#     unless => "/bin/grep -q 'wgReadOnlyFile =' $cfg",
#   }

#   file { "/usr/local/bin/stage-mediawiki":
#     content => template("s_wiki/backup.erb"),
#     mode    => '0755',
#   }

#   cron { "wikistage":
#     command => "/usr/local/bin/stage-mediawiki",
#     user    => "root",
#     hour    => 1,
#     minute  => 3,
#   }

#   nagios::check_file_age { "/usr/share/mediawiki-stage/mediawiki.xml":
#     warning             => 90000,
#     critical            => 176400,
#     service_description => "backup stage age",
#   }
# }
