class s_adduser {
  file { "/var/yp/adduser":
    content => template("s_adduser/adduser.erb"),
    mode    => '0555',
  }
  file { "/var/yp/adduser-to-group":
    content => template("s_adduser/adduser-to-group.erb"),
    mode    => '0555',
  }
  file { "/var/yp/change-password":
    content => template("s_adduser/change-password.erb"),
    mode    => '0555',
  }
  file { "/var/yp/push.ryttis":
    content => template("s_adduser/push.ryttis.erb"),
    mode    => '0555',
  }
  file { "/var/yp/processpass.py":
    content => template("s_adduser/getpass.erb"),
    mode    => '0444',
  }
}
