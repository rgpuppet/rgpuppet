class s_rt {
  include rt

  file { "/etc/request-tracker3.8/RT_SiteConfig.d/rg.pm":
    content => template("s_rt/rg.pm.erb"),
    notify  => Exec["update-rt-siteconfig"],
  }
}
