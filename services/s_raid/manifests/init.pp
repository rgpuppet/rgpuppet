class s_raid {
  include nagios::nrpe
  include nagios::plugins_standard

  $need_check_linux_raid = $lsbdistrelease ? {
    "8.04"  => true,
    "8.10"  => true,
    default => false
  }

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    $checker = "/usr/lib/nagios/plugins/check_linux_raid"
  }
  else {
    include nagios::plugins_contrib

    $checker = "/usr/lib/nagios/plugins/check_raid"
  }

  if $need_check_linux_raid {
    file { "$checker":
      content => template("s_raid/check_linux_raid.erb"),
      mode    => '0755',
    }
  }

  file { "/etc/nagios/nrpe.d/s_raid.cfg":
    content => "command[check_raid]=$checker\n",
    notify  => Service[nagios-nrpe-server],
  }

  @@nagios_service { "$hostname raid":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "raid",
    check_command       => "check_nrpe_1arg!check_raid",
    stalking_options    => "w,c",
    target              => rg_nagios_target("raid"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}
