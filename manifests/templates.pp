class internal_dns {
  network::resolvconf { "ryttargarden":
    servers     => [$ns_resolver_1, $ns_resolver_2],
    extrasearch => [],
    tag         => "bootstrap",
  }
}

class dmz_dns {
  network::resolvconf { "ryttargarden":
    servers     => [$ns_resolver_1, $ns_resolver_2],
    extrasearch => [],
    tag         => "bootstrap",
  }
}

class dmz_2_dns {
  network::resolvconf { "ryttargarden":
    servers     => [$ns_resolver_dmz_2_1, $ns_resolver_dmz_2_2],
    extrasearch => [],
    tag         => "bootstrap",
  }
}

class stdnet {
  include network::fromdns
  include internal_dns
}

class dmznet {
  include network::fromdns_dmz
  include dmz_dns
}

class dmz_2_net {
  include network::fromdns_dmz_2
  include dmz_2_dns
}

class baseservices {
  include ssh::server
  include emacs

  # Ensure something installs the puppet client.  Unfortunately, we
  # must install puppet or puppet::master, and they cannot coexist, so
  # we cannot include puppet here...
  Service['puppet'] -> Class['emacs']

  include nagios::nrpe
  include ntpd
  include screen
  include man
  include rsync
  include s_rwho
  include wget
  include s_easy_upgrade
  include enable_fsck
  include nagios::swap
  include s_nullclient
  include logrotate
  include logrotate::keep_wtmp
  include sysstat
  include tcpdump

  $eolrelease = $lsbdistrelease ? {
    "8.10"  => true,
    "9.04"  => true,
    default => false,
  }

  $mirror = $eolrelease ? {
    true => "http://old-releases.ubuntu.com/ubuntu/",
    false => "http://se.archive.ubuntu.com/ubuntu/"
  }

  debmirror::usemirror { "$mirror":
    tag        => "bootstrap",
    eolrelease => $eolrelease,
  }

  exec { "/usr/bin/apt-get update":
    schedule => "daily",
  }

  $have_mirror = true
  if $have_mirror {
    file { "/etc/apt/apt.conf":
      content => "Acquire::http::Proxy \"http://hiskia.ryttargarden:3142/\";\n",
      notify => Exec['update-apt'],
      tag    => "bootstrap",
    }
  }
  else {
    file { "/etc/apt/apt.conf":
      ensure => absent,
      notify => Exec['update-apt'],
      tag    => "bootstrap",
    }
  }

  include syslog

  case $operatingsystem {
    'Ubuntu': {
      exec { "/usr/sbin/locale-gen sv_SE.UTF-8":
        unless => "/usr/bin/locale -a|grep '^sv_SE.utf8\$'",
      }
    }
    'Debian': {
      exec { "/bin/echo sv_SE.UTF-8 UTF-8 >> /etc/locale.gen":
        unless => "/bin/grep -q '^sv_SE.UTF-8 UTF-8' /etc/locale.gen",
        notify => Exec["/usr/sbin/locale-gen"],
      }
      exec { "/usr/sbin/locale-gen":
        refreshonly => true,
      }
    }
  }
}

class physicalservices {
  include baseservices
  include hddtemp
  include smartmontools
}

class virtualnode {
  include baseservices
  include linux_virtual
  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    include noacpi
  }
}
