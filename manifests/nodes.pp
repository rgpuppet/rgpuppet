node salomo {
  include physicalservices
  include puppet
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend      => "KVM server 1",
    switch      => "switch-1104",
    switch_port => "1",
    parent      => "switch-1104",
    hostgroup   => "servers",
    category    => 'kvm',
  }

  smartmontools::nagios { "sda": }
  smartmontools::nagios { "sdb": }

  hddtemp::check { "sda":
    warn => 32,
    crit => 35,
  }

  hddtemp::check { "sdb":
    warn => 32,
    crit => 35,
  }

  include bonnie
  include kvm
  include s_router
  include s_cups
  include s_raid
  include nut
#  nut::master { "eaton":
#    driver => "usbhid-ups",
#    listen => $ipaddress_br0,
#    port   => "auto",
#  }
}

node josua {
  include physicalservices
  include puppet
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend      => "KVM server 2",
    switch      => "switch-1104",
    switch_port => "2",
    parent      => 'switch-1104',
    hostgroup   => "servers",
    category    => ['kvm', 'router'],
  }

  smartmontools::nagios { "sda": }
  smartmontools::nagios { "sdb": }

  hddtemp::check { "sda":
    warn => 35,
    crit => 38,
  }

  hddtemp::check { "sdb":
    warn => 35,
    crit => 38,
  }

  include kvm
  include s_router
  include s_cups
  include s_raid
}

node pcfritz {
  include physicalservices
  include stdnet
  include puppet

  # Also see comments in s_backup_server.
  include s_backup_server
  include s_nis

  class { 'host':
    legend      => "LYSrdiff backup",
    switch      => "switch",
    switch_port => "18",
    parent      => "switch",
    hostgroup   => "servers",
    category    => 'backup',
  }

  smartmontools::nagios { "sda": }

  hddtemp::check { "sda":
    warn => 48,
    crit => 53,
  }

  # Also create ssh key:
  #   sudo ssh-keygen -C "LYSrdiff @ pcfritz" -f /root/.ssh/backupkey  -q
  # Copy the key value into the field below:

  @@ssh_authorized_key { "LYSrdiff @ pcfritz":
    options => ["from=\"$ipaddress\"",
                "no-port-forwarding",
                "no-x11-forwarding",
                "no-agent-forwarding"],
    user    => "root",
    type    => "rsa",
    key     => "AAAAB3NzaC1yc2EAAAABIwAAAQEAsouestRUWRJ6eMdON/1JTeB74MHfmTkwGMd8RkwGfAknU+OckLuTYnftMNe/PxG2rW8FwROWtCWV7xpMHBQGKASlYygqTeZFnMYVi0OqBW06On9H2Diy6W08Ebz/j3dIPvdLCpQD4oOQfJDvWgTP+3E6csIuhNrQO5qNQXQ7BKYuLsYJNueHU7v6ltRdKC0xGk3XqVsI1TQfYJZWxn7KF4jF7zABHYwiUo8/MPzwrrGwVcJ5hk46uPDW8JfSHM4LGjNU/y0Qf2HsVBskdvc+5RXTqy49YwFWfbGWjZ71ZzHNJpV3f17+KHf5hBlnxxxXossWHc9NoaEeg3Z6sBS+PQ=="
  }
}

node abigail {
  include virtualnode
  include stdnet
  include s_users::sysadmins_local_opt
  include puppet

  class { 'host':
    legend    => 'LTSP server',
    parent    => 'reguel',
    hostgroup => "virtual_servers",
    category  => 'ltsp',
  }

  # Edit /usr/lib/ruby/1.8/facter/domain.rb and comment out the call
  # to domainname.  (The code should only use dnsdomainname.)

  include s_workstation
  include s_ltsp
  include s_ltsp::ltsconf
  include kickstart
  include shotwell

  include s_development

  # Install Oracle Java to get the right version of the Java browser
  # plugin for usage with our alarm/lock system.
  #
  # The JDK needs to be downloaded separately and placed in
  # /var/cache/oracle-jdk8-installer, since our firewall seem to block
  # some of the redirects required to download the Oracle Java archive
  # automatically by the oracle-java8-installer (I have not
  # investigated that further /Andreas 16-04-08)
  #
  # sudo add-apt-repository ppa:webupd8team/java
  # sudo apt-get update
  # sudo apt-get install oracle-java8-installer
}

node alva {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => "DNS slave",
    parent    => 'josua',
    hostgroup => "virtual_servers",
    category  => 'dns',
  }

  include swapfile
  include s_dns::slave
  include ssh::server
  include bind
}

node symeon {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'Nagios and Arpwatch',
    hostgroup => 'virtual_servers',
    parent    => 'shifra',
    category  => 'nagios',
  }

  dnseq::check { "ryttargardskyrkan.se":
    expected => "ryttargarden.dyndns-ip.com",
  }

  include s_nagios
  include dhcpd::monitor
  include s_cups
  include s_development
  include apache
  include apache::ssl
  apache::certificate { '/etc/ssl/private/nagios.key':
    cert  => '/etc/ssl/nagios/nagios.crt',
    chain => '/etc/ssl/nagios/chain.pem',
  }
  include arpwatch

  # Also install nagios-plugins from Lysator:
  # 	 git clone https://git.lysator.liu.se/lysator/nagios-plugins.git
  # 	 cd nagios-plugins
  # 	 sudo make install
  #
  # Also install the check_ypmatch plugin:
  #
  #      git clone git@git.lysator.liu.se:check_ypmatch/check_ypmatch.git
  #      cd check_ypmatch
  #      make
  #      sudo make install
  #
  # Add users using:
  #
  #  htpasswd /etc/nagios3/htpasswd.users $USER
  #
  # Also run
  #
  #       sudo graphite-manage syncdb
  #
  # Follow the manual instructions in modules/nagios/manifests/graphios.pp.
}

node judas {
  # Networking set up manually.
  include s_users::sysadmins
  include puppet

  class { 'host':
    legend      => "Ljudgruppens filserver",
    switch      => "switch-1104",
    switch_port => 6,
    hostgroup   => "servers",
    parent      => "switch-1104",
    category    => 'audio fileserver',
  }

  smartmontools::nagios { "sda": }
  smartmontools::nagios { "sdb": }
  smartmontools::nagios { "sdc": }
  smartmontools::nagios { "sdd": }

  hddtemp::check { "sda":
    warn => 30,
    crit => 33,
  }

  hddtemp::check { "sdb":
    warn => 30,
    crit => 33,
  }

  hddtemp::check { "sdc":
    warn => 33,
    crit => 36,
  }

  hddtemp::check { "sdd":
    warn => 33,
    crit => 36,
  }

  include physicalservices
  include s_raid
  include s_denyhosts
  include s_lysrdiff::access
}

node dina {
  include virtualnode
  include puppet
  include network::multi
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend    => 'DHCP slave',
    parent    => 'shifra',
    hostgroup => 'virtual_servers',
    category  => 'dhcp',
  }

  include swapfile

  include dhcpd
}

node jemima {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'MediaWiki',
    parent    => 'josua',
    hostgroup => "virtual_servers",
    category  => 'wiki',
  }

  include swapfile

  include s_wiki
  include s_development

  file { "/opt/LYSrdiff":
    ensure => directory,
    owner  => ceder,
  }

  include s_lysrdiff::access
  include ssh::permit_root
}

node gad {
  include virtualnode
  include stdnet
  include s_users::sysadmins_local_opt
  include noacpi

  class { 'host':
    legend    => 'File server',
    parent    => 'elia',
    hostgroup => 'virtual_servers',
    category  => 'home',
  }

  include s_diskserver
  include logrotate
  include lvm

  include s_lysrdiff::access
  Ssh_authorized_key <<| title == "adduser @ sheva" |>>
  Ssh_authorized_key <<| title == "rsync @ ackuv" |>>

  # Manually create a key in ~root/.ssh/id_rsa, and add the public key here.
  @@ssh_authorized_key { 'rsync @ gad':
    options => ["from=\"$ipaddress\"",
                'no-port-forwarding',
                'no-x11-forwarding',
                'no-agent-forwarding'],
    user    => 'root',
    type    => 'rsa',
    key     => 'AAAAB3NzaC1yc2EAAAADAQABAAAEAQDTLobwEuMWn6deLoc8uhVMJ79aX+9SV3d40zmmi9+fShCoxm022NtY0xHhRDk8ostAocMGzcaaqh9cTlMVBozgpcQU5iUOnd33Xl24bqXODbjznZc7QCxHTXehPz91nip8LoK27qNisOZvyY7AEs4gQ/bM7mCoqzAZRPBT5n523vTbOHL5b/WIKuDGGEsWpLuA+Y/8gExwOls87fnjB5NTyjWecC7FMkLo8CMIs7A6mw+qOEkWORVqmuCWO3QpS/pUYbNvuLGNGJlUrXg4bGw/C8naY8vkstQ+AvTrjjSpzpFbl4ZBYDe1nstTOfvd6EKziKV6A4PI51KSiROXrstEBSha3nPi1Yljh+MHj/+Zc6vLPFvjbmzJC4MAd0f50O5vgW2LOKvbeyQjmTGz+7ELBN2mjiW1uu0CMjp6YUVPEPQn1C3AVGIWlsVqdpe/eg4fyNLJzGdSQ/h1RxkrB7o+Vqig+BoJGs3poiIiD6m6rLZZfjS4hgw5B/b1kb9najVDYAsZr/LaPCfM5MR4tM7d8Lwu3Z05ZTU6L2Os8Ig3cnDAyF0xsmahTq48fkwDVqHYN06XE6KTe+5sj4uJbI30pMbug8UeI2uU920H7jaeQ3OthcLMZPznV9kF/Ja7b1W4JhHCOCutByNHQ2PXODiLlGeojE4fsmFmeJeiO0YF8PQFx9FEbmkwGI0SpgbV/63pJT4fxRQIU/qp3yJFJwO9HrGRb1uTsCxq1wf9es7JoZ/DL9IGd493iFkYQLkM2JzReq6g4R3A/t4rTYNQz5/8fEu69IZ2CPHKi2S/VHQE5XcD1XAiwMvOwvxCc5Q+47Goapw+QISy8SPwy+DqXz5VK962cxQLEE2x/H3QORWyCtD/yIRB4UfJImUGIIeMcwwQpXB0mAJ9lNXVfIOQP9LqeUKdOcp3YvIs4we39It5E1GHCwLLihfFzS9lyyqDQ2k30Of3SNSB1f/G+qHdg9HFXZ30LdjTxYSCtQIbMkYy75Ji1Bro1DD/BPB7Ea9UqvsL1ua6RgR6nZtz9JIxN2RvPJ6grrDuZw4mCGeE8boZNQDjj/HG0bfqh2BDAp3cJlJh2P7bhbM+XW32o5T0kVp9tTNsJ7xkJp1XHQU9UtqFWJOX/DM98jXGqbVsGsYdRxQb9VrBZeVGfqGOEMplS0kUelD5naYDPJieiDO8a7wuI74eqlBnFHmmMfQMR/1Dzj3HUONEbn/NhUk6yUPuVoVXrAcX6fpO4GcGmuptsY6F9H2u882EeDW2wnUbvHZ5FLx14mfv5HRaS0PO0k5l535s4HZ4jHk0ZHjaaLYiJ4rLiRCrjGtQFTJ9D8Xt0RyOJPheouc+g+TbG9ueWUk80zJH'
  }

  file { '/usr/local/sbin/mirror-disks':
    ensure => absent,
  }
}

node lydia {
  include virtualnode
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'YP slave',
    parent    => 'josua',
    hostgroup => "virtual_servers",
    category  => 'yp',
  }

  include swapfile

  include ssh::server
  include puppet
  include ssh::permit_root
  include emacs
  include nis
  nis::server { "ryttargarden.nis":
    servertype => "slave",
    nismaster  => "sheva.ryttargarden",
    checkuser  => "ceder",
  }
  # Also manual configuration i /var/yp/Makefile,
  # /var/yp/ypfiles/* (including auto.*, passwd, group, shadow).
}

node bigvaj {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'RT server',
    parent    => 'josua',
    hostgroup => "virtual_servers",
    category  => 'rt',
  }

  include ssh::server
  include s_rt
}

node johannes {
  $ntp_servers = $dmz_ntp_servers

  include physicalservices
  include dmznet
  include s_users::sysadmins

  class { 'host':
    legend      => 'Infoscreen on DMZ',
    switch      => "switch",
    switch_port => 20,
    hostgroup   => "servers",
    parent      => "switch",
    category    => 'infoscreen',
  }

  nagios::disk { "/": }

  smartmontools::nagios { "sda": }

  hddtemp::check { "sda":
    warn => 35,
    crit => 38,
  }

  include ssh::server
  include puppet
  include s_denyhosts

  ssh::allow_users { "andvin ceder emil mathz":
  }
}

node adam {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins_local_opt

  class { 'host':
    legend    => 'print server',
    parent    => 'elia',
    hostgroup => "virtual_servers",
    category  => 'print',
  }

  include s_cups::server
  include s_nis
}

node piltaj {
  class { 'host':
    legend    => 'Puppet master',
    hostgroup => 'virtual_servers',
    parent    => 'reguel',
    category  => 'puppet',
  }

  include virtualnode
  include stdnet
  include s_users::sysadmins

  include s_git_user
  include puppet::master
  include apache
  include ubuntu_supported
}

node sheva {
  class { 'host':
    legend    => 'YP master',
    hostgroup => 'virtual_servers',
    parent    => 'shifra',
    category  => 'yp',
  }

  include virtualnode
  include stdnet
  include s_users::sysadmins

  include ssh::server
  include puppet
  include ssh::permit_root
  include emacs
  include build_essential
  include gcc
  include nis
  nis::server { "ryttargarden.nis":
    servertype => "master",
    checkuser  => "ceder",
  }
  include s_adduser
  # Also manual configuration i /var/yp/Makefile,
  # /var/yp/ypfiles/* (including auto.*, passwd, group, shadow).

  # Also install LYSrdiff.

  # Also create /root/.ssh/id_rsa_adduser by running ssh-keygen.
  # Enter the public key here.
  @@ssh_authorized_key { "adduser @ sheva":
    options => ["from=\"$ipaddress\"",
                "no-port-forwarding",
                "no-x11-forwarding",
                "no-agent-forwarding"],
    user    => "root",
    type    => "rsa",
    key     => "AAAAB3NzaC1yc2EAAAADAQABAAABAQClq3hRhhD5kNjKZn8xNH4KqYiw0sNTyVaQhsLsHeNalnwL2C7+seQeh/2ir32CZFAUhfjwaTgEUnglD4axcavqN/sRJpV+kfAdS7MYPJi3xj19vWMKedA09M/3w7CWe63VwWntQuimUx4sxz5fNqUpQEEcF3uOJf+nWiFXp0aLE7VbS7MZOGjPuGUqfuliVi4f2YVLWUIYFTXfteUdwVAYu1SwsEwAFAt0zE19QSh9kAPkEwY4Mx9y6D3ng6w+rI/2LdmpkCDYo2uMu1uFbTgxHkGOQR0MqcxQoMLksCIM3PdVU6sTFd+mOKEmYE2DBxiJm/cgR2ZlPLoPcmozG8Gn",
  }

  # Also copy the adduser @ sheva key to
  # david:~root/.ssh/authorized_keys so adduser
  # can update /etc/mail/ryttargarden.canonical.users.

  include s_lysrdiff::access
}

node shallum {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins
  include s_users::firewall_users

  class { 'host':
    legend    => 'SSH server',
    parent    => 'josua',
    hostgroup => 'virtual_servers',
    category  => 'ssh',
  }

  include swapfile

  ssh::allow_users { 'ceder emil andvin samfors ingrid mathz': }

  include ssh::forbid_root
  include ssh::forbid_password

  include s_denyhosts
}

node ackuv {
  include virtualnode
  include stdnet
  include s_users::sysadmins_local_opt

  class { 'host':
    legend    => 'Hot standby file server',
    parent    => 'josua',
    hostgroup => 'virtual_servers',
    category  => 'home',
  }

  include s_diskserver
  include logrotate
  include lvm

  include s_lysrdiff::access

  # Manually create a key in ~root/.ssh/id_rsa, and add the public key here.
  @@ssh_authorized_key { 'rsync @ ackuv':
    options => ["from=\"$ipaddress\"",
                'no-port-forwarding',
                'no-x11-forwarding',
                'no-agent-forwarding'],
    user    => 'root',
    type    => 'rsa',
    key     => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDENEHs80CYi67ulEwlaxNbiGiPnAG8KctXdFsIeyiZdKk5rqHvWNuc7gMyc7RmBcH1cLH7xqkUU8w+XWhm3KVJ/5RB6bA5zYUnsY8VyRsGfvR5a/YVXlA8DnkrB3ST+d70WdXxvdDKnRQAD++QmJ36CTxvPwzl2aRC4MJdnx2hILkjQ9t3yJ+/WUL+WGVP8j6FPY34mO2FWGD9HzJO40qlaVCf42p2+XTu4EA94AWhujg90K68QIXYOG8PqzI1v1YZaF9oc2zI61X4IxH6bFOUjZe2h983dnfttMe32/e4Zs5ZTff+LTX7L+v2eBlHZY9pfPDDKpxlFLgGGzdKR6CH'
  }

  file { '/usr/local/sbin/mirror-disks':
    content => "#!/bin/sh\nrsync -a --partial -H -S --delete --force gad:/samba/ /samba&&rsync -a --partial -H -S --delete --force gad:/home/ /home&&touch /var/local/mirror-ok\n",
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
  }

  include cron

  cron { 'mirror-disks':
    command => '/usr/local/sbin/mirror-disks',
    user    => 'root',
    hour    => [0, 6, 12, 18],
    minute  => 4,
  }

  nagios::check_file_age { '/var/local/mirror-ok':
    warning             => 46800,
    critical            => 87000,
    service_description => 'disk mirror age',
  }

  mountpoint { "/home":
    label => "home",
  }

  mountpoint { "/samba":
    label => "samba",
  }
}

node shemaja {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'DNS master',
    parent    => 'shifra',
    hostgroup => 'virtual_servers',
    category  => 'dns',
  }

  include swapfile
  include ssh::server
  include ssh::permit_root
  include emacs
  include bind
  include s_bind_push_to_david
  include s_dns::master

  include s_lysrdiff::access

  # Also install LYSrdiff.
}

node seraja {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'Syslog server',
    parent    => 'reguel',
    hostgroup => 'virtual_servers',
    category  => 'syslog',
  }

  include swapfile

  mountpoint { '/syslog':
    label => 'syslog-data',
  }
}

node achia {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'Syslog server',
    parent    => 'josua',
    hostgroup => 'virtual_servers',
    category  => 'syslog',
  }

  mountpoint { '/syslog':
    label => 'syslog-data',
  }
}

node elia {
  include physicalservices
  include puppet
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend      => "KVM server 3",
    switch      => "switch-1104",
    switch_port => "5",
    hostgroup   => "servers",
    category    => 'kvm',
  }

  smartmontools::nagios { "sda": }
  smartmontools::nagios { "sdb": }

  hddtemp::check { "sda":
    warn => 35,
    crit => 38,
  }

  hddtemp::check { "sdb":
    warn => 35,
    crit => 38,
  }

  include kvm
  include s_router
  include s_cups
  include s_raid
}

node hiskia {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => "Apt cache",
    parent    => 'shifra',
    hostgroup => "virtual_servers",
    category  => 'apt cache',
  }

  include swapfile

  include aptcacher
}

node joav {
  include virtualnode
  include puppet
  include stdnet

  # Also see comments in s_backup_server.
  include s_backup_server
  include s_nis

  class { 'host':
    legend    => 'LYSrdiff backup',
    parent    => 'salomo',
    hostgroup => 'virtual_servers',
    category  => 'backup',
  }

  include swapfile

  # Also create ssh key:
  #   sudo ssh-keygen -C "LYSrdiff @ joav" -f /root/.ssh/backupkey  -q
  # Copy the key value into the field below:

  @@ssh_authorized_key { "LYSrdiff @ joav":
    options => ["from=\"$ipaddress\"",
                "no-port-forwarding",
                "no-x11-forwarding",
                "no-agent-forwarding"],
    user    => "root",
    type    => "rsa",
    key     => "AAAAB3NzaC1yc2EAAAADAQABAAABAQDh6QXa/CJMHrA+GqZEUyxwtwQSUAqs+UqnyfTVb/6neY0LJqSoIYIVnvKb6HlVFVlHfreweAOP0QMhnTZawnUEUjXdnTAc/PluBZsWEMJCCSxDnOnZteTIW7CZeutWy7vtFSwY0n4Fx0NC5pI94KSwpmytKXW3rdwlzLywptmOJ8Xus3tG8J1RITfG5wFJmxLj33GMSBtBJP7p1qaBt6L6uzd2/++810r+nFhOl67pab8T59ASG3IE1wku386jHh3lsvFokwjO3Q0TCgHOUkcQv85/PFlMnCR2WeeZaZ0RtGDG0tdbnPFG5lDo3Wqpk+t5a6xvlTGdCnGBN3YOJM5r"
  }
}

node jakob {
  include virtualnode
  include puppet
  include network::multi
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend    => 'DHCP master',
    parent    => 'josua',
    hostgroup => 'virtual_servers',
    category  => 'dhcp',
  }

  include swapfile

  include dhcpd
}

node puva {
  include virtualnode
  include puppet
  include network::multi
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend    => 'DNS resolver',
    parent    => 'pua',
    hostgroup => 'virtual_servers',
    category  => 'dnsresolv',
  }

  include swapfile

  include unbound
}

node tola {
  include virtualnode
  include puppet
  include network::multi
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend    => 'DNS resolver',
    parent    => 'josua',
    hostgroup => 'virtual_servers',
    category  => 'dnsresolv',
  }

  include swapfile

  include unbound
}

node mirjam {
  include virtualnode
  include puppet
  include s_users::sysadmins

  class { 'host':
    legend    => 'mp3wavweb server',
    parent    => 'pua',
    hostgroup => 'virtual_servers',
    category  => 'mp3wavweb',
  }

  # This virtual server provides a small web app that authorized
  # persons can use do download recordings, and optionally convert
  # them to mp3 before doing so.

  # Clone git@git.lysator.liu.se:mp3wavweb/mp3wavweb.git
  # and run "make && sudo make install && service apache2 restart"
  # to install the web service.

  include apache::wsgi
  include lame
  include git
  include make

  file { '/ljud':
    ensure => directory,
  }

  mount { '/ljud':
    require  => File['/ljud', '/etc/judas.creds'],
    ensure   => defined,
    atboot   => true,
    device   => '//judas/ljud',
    dump     => 0,
    fstype   => 'cifs',
    options  => 'credentials=/etc/judas.creds,_netdev,noperm',
    pass     => 0,
    remounts => true,
  }

  $cifs_username = hiera('cifs::judas::user')
  $cifs_password = hiera('cifs::judas::password')

  file { '/etc/judas.creds':
    content => "username=${cifs_username}\npassword=${cifs_password}\n",
    owner   => 'root',
    group   => 'root',
    mode    => '0400',
  }

  $htpasswd_password = hiera('web::mirjam::audio::encrypted')
  file { '/etc/apache2/htpasswd.users':
    content => "audio:${htpasswd_password}\n",
  }
}

node simon {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins_local_opt

  class { 'host':
    legend    => 'New domain controller being set up',
    parent    => 'pua',
    hostgroup => 'virtual_servers',
    category  => 'ad?',
  }

  include swapfile

  include s_dc
}

node levi {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'New ChurchInfo server',
    parent    => 'reguel',
    hostgroup => 'virtual_servers',
    category  => 'churchinfo',
  }

  include swapfile
# FIXME: Temporarily disabled to make it safe to run puppet:
#  include s_register
# FIXME: Temporarily disabled to make it safe to run puppet:
#  include s_development

  # Needed?
  include ssh::server

  # Moved to modules/churchinfo
#  include mysql::server
#  include php5
#  include apache
# FIXME: Temporarily disabled to make it safe to run puppet:
#  include vc::subversion

  # Config missing:
  # - Set up backup of MySQL db.
  # - Set up mailserver. Forward mails sent
  #    to eg. registersvar@ryttargardskyrkan.se to
  #    registersvar@levi.ryttargardskyrkan.se

  # Note:
  # - The files in /var/www are owned by root.

  # After reinstallation of this server, the following must be done:
  # - Check out the source code for ChurchInfo to /var/www.
  # cd /var/www && git clone homes.ryttargarden:/home/emil/.git-repos/churchinfo.git register
  # - Set decent user and group on those files!
  #
  # Set passwords in churchinfo config files.
  # - Load database dump, or at least the schema.
  #    from where?
  # Create user in mysql db.
  #   GRANT ALL ON infocentral.* TO 'secretuser'@'localhost' IDENTIFIED BY 'somepass';
  # - Copy mailreply scripts. From where? Svn?

  # Also install LYSrdiff.
  include s_lysrdiff::access
  include ssh::permit_root
}

node isaskar {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'Ubuntu 16.04 test',
    parent    => 'reguel',
    hostgroup => 'virtual_servers',
    category  => 'unused',
  }
}

node sebulon {
  include virtualnode
  include puppet
  include network::multi
  include internal_dns
  include s_users::sysadmins_local_opt

  class { 'host':
    legend    => 'print server',
    parent    => 'pua',
    hostgroup => 'virtual_servers',
    category  => 'print',
  }

  include s_cups::server
  include s_nis
}

node naftali {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'Squid',
    parent    => 'josua',
    hostgroup => 'virtual_servers',
    category  => 'squid',
  }

  include s_squid
}

node asher {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins
  include s_users::firewall_users

  class { 'host':
    legend    => 'SSH server',
    parent    => 'reguel',
    hostgroup => 'virtual_servers',
    category  => 'ssh',
  }

  ssh::allow_users { "ceder emil andvin samfors ingrid mathz":
  }

  include ssh::forbid_root
  include ssh::forbid_password

  include s_denyhosts
}

node josef {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'Outgoing SMTP smarthost',
    parent    => 'pua',
    hostgroup => 'virtual_servers',
    category  => 'smtp',
  }

  include swapfile

  include s_nullclient
}

node gershom {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins_local_opt

  class { 'host':
    legend    => 'Emergency david replacement; RGXP domain controller',
    parent    => 'elia',
    hostgroup => 'virtual_servers',
    category  => 'samba',
  }

  include s_rgxp_server
}

node shifra {
  include physicalservices
  include puppet
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend      => 'KVM server 4',
    enclosure   => '1',
    switch      => "switch-netgear",
    switch_port => "1",
    parent      => 'switch-netgear',
    hostgroup   => 'servers',
    category    => 'kvm',
  }

  smartmontools::nagios { 'sda': }
  smartmontools::nagios { 'sdb': }

  hddtemp::check { 'sda':
    warn => 35,
    crit => 38,
  }

  hddtemp::check { 'sdb':
    warn => 35,
    crit => 38,
  }

  include kvm
  include s_router
  include s_cups
  include s_raid
}

node pua {
  include physicalservices
  include puppet
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend      => 'KVM server 5',
    enclosure   => '1',
    switch      => "switch-netgear",
    switch_port => "2",
    parent      => 'switch-netgear',
    hostgroup   => 'servers',
    category    => 'kvm',
  }

  smartmontools::nagios { 'sda': }
  smartmontools::nagios { 'sdb': }

  hddtemp::check { 'sda':
    warn => 35,
    crit => 38,
  }

  hddtemp::check { 'sdb':
    warn => 35,
    crit => 38,
  }

  include kvm
  include s_router
  include s_cups
  include s_raid
}

node reguel {
  include physicalservices
  include puppet
  include internal_dns
  include s_users::sysadmins

  class { 'host':
    legend      => 'KVM server 6',
    enclosure   => '1',
    switch      => "switch-netgear",
    switch_port => "3",
    parent      => 'switch-netgear',
    hostgroup   => 'servers',
    category    => 'kvm',
  }

  smartmontools::nagios { 'sda': }
  smartmontools::nagios { 'sdb': }

  hddtemp::check { 'sda':
    warn => 35,
    crit => 38,
  }

  hddtemp::check { 'sdb':
    warn => 35,
    crit => 38,
  }

  include kvm
  include s_router
  include s_cups
  include s_raid
}

node sippora {
  include physicalservices
  include puppet
  include s_router
  include s_users::sysadmins

  class { 'host':
    legend      => 'New physical server with nonredundant disks',
    enclosure   => '1',
    switch      => "switch-netgear",
    switch_port => "4",
    parent      => 'switch-netgear',
    hostgroup   => 'servers',
    category    => 'unused',
  }

  smartmontools::nagios { 'sda': }

  hddtemp::check { 'sda':
    warn => 35,
    crit => 38,
  }

  # sippora does not have an sdb disk, so it doesn't use raid.
  # include s_raid
}

node jetro {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins

  class { 'host':
    legend    => 'CA',
    parent    => 'salomo',
    hostgroup => 'virtual_servers',
    category  => 'ca',
  }

  include swapfile

  include s_ca

  # Also follow these steps, based on the guide at
  # https://help.ubuntu.com/lts/serverguide/openvpn.html.
  #
  # Create CA cert:
  #
  #   cd /etc/openvpn/easy-rsa/
  #   source vars
  #   ./clean-all
  #   ./build-ca
  #
  # Create server cert et c for gideon:
  #
  #   cd /etc/openvpn/easy-rsa/
  #   source vars
  #   ./build-key-server ryttargardskyrkan
  #   ./build-dh
  #   cd keys
  #   scp ryttargardskyrkan.crt ryttargardskyrkan.key ca.crt dh2048.pem ceder@gideon:
  #
  # Create a client certificate (as root):
  #
  #   cd /etc/openvpn/easy-rsa/
  #   source vars
  #   ./build-key client1
  #
  #   You then need to copy these files to the client:
  #
  #       /etc/openvpn/easy-rsa/keys/ca.crt       (tamperproof copy!)
  #       /etc/openvpn/easy-rsa/keys/client1.crt  (tamperproof copy!)
  #       /etc/openvpn/easy-rsa/keys/client1.key  (secret!)
}

node gideon {
  include virtualnode
  include puppet
  include stdnet
  include s_users::sysadmins_local_opt

  class { 'host':
    legend    => 'OpenVPN server',
    parent    => 'salomo',
    hostgroup => 'virtual_servers',
    category  => 'vpn',
  }

  include swapfile

  include s_openvpn

  # Move the CA-related file that were created on jetro to the proper
  # place:
  #
  #    cd ~ceder
  #    mv ryttargardskyrkan.crt ryttargardskyrkan.key ca.crt dh2048.pem \
  #       /etc/openvpn
  #    chown root /etc/openvpn/ryttargardskyrkan.key
}

node magdalena {
  include virtualnode
  include puppet
  include dmz_2_net
  include s_users::sysadmins
  include s_users::www_users

  class { 'host':
    legend    => 'WWW server',
    parent    => 'elia',
    hostgroup => 'virtual_servers',
    category  => 'www',
  }

  include apache
  include mysql::server
}

node esra {
  # esra is a virtual machine running at Per Cederqvists home, used
  # for off-site backups.  It connects using openvpn.

  include virtualnode
  include puppet

  # Also see comments in s_backup_server.
  include s_backup_server

  class { 'host':
    legend      => 'LYSrdiff offsite backup',
    parent      => 'gideon',
    hostgroup   => 'virtual_servers',
    category    => ['backup', 'offsite'],
    ip_override => split(generate('/usr/bin/getent', '--service=dns',
                                  'hosts', $hostname), ' '),
  }

  include swapfile

  # Also create ssh key:
  #   sudo ssh-keygen -C "LYSrdiff @ esra" -f /root/.ssh/backupkey  -q
  # Copy the key value into the field below:

  @@ssh_authorized_key { "LYSrdiff @ esra":
    options => [sprintf("from=\"%s\"", rg_dns("esra")),
                "no-port-forwarding",
                "no-x11-forwarding",
                "no-agent-forwarding"],
    user    => "root",
    type    => "rsa",
    key     => "AAAAB3NzaC1yc2EAAAADAQABAAABAQCrL36y0bUSqtzC9aPnDQrCPf26rByV6D3S3wMpookLgJisv6zlcjPW4ZRKJfZQpURMndsXfiO37KWT4/Jcm4XJd3xCkEmx8J/qsLm7LLwTjlyRPqQyuQyO2OmrkBKdQcLj7SAWXh+vzZ/NA835oaqKiwBFYiIQpUKr9qLxLKboIRhx/7LCRrmaeU174IpEw3EYKt3u/oqdrW5rVwxQsRTyxpTi7MPR2ILhckmuJrN/rI9PNi/6ut7p6B8/Ptzgbn0ALKdvcTKyD1SkJIggW31qWjFRks4140riINbStizvJT7CFyK9dKP5HfjdwgrO8CvyLRYHcpuDLL9L+ZJY9SLZ"
  }
}
