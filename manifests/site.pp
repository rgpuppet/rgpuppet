$gateway = '192.168.10.254'
$dmz_gateway = '192.168.0.254'
$dmz_2_gateway = '192.168.13.254'
$ns1 = '192.168.10.141'
$ns2 = '192.168.10.126'
$ns_resolver_1 = '192.168.10.146'
$ns_resolver_2 = '192.168.10.147'
$ns_resolver_dmz_2_1 = '192.168.13.246'
$ns_resolver_dmz_2_2 = '192.168.13.247'
$office_network = '192.168.10.0'
$office_mask = '255.255.255.0'
$nagios_hosts = ['192.168.10.149']
$ntp_servers = ['josua.ryttargarden',
                'elia.ryttargarden',
                'salomo.ryttargarden']
$dmz_ntp_servers = ['dmz-josua.ryttargarden',
                    'dmz-elia.ryttargarden',
                    'dmz-salomo.ryttargarden']
$main_router = 'josua'
$external_ntp_servers = ['ntp1.sth.netnod.se',
		         'ntp2.sth.netnod.se',
			 'ntp1.gbg.netnod.se',
			 'timehost.lysator.liu.se']
$puppetmaster = 'piltaj.ryttargarden'

#  8.04 LTS hardy
#  8.10     intrepid
#  9.04     jaunty
#  9.10     karmic
# 10.04 LTS lucid
# 10.10     maverick
# 11.04     natty
# 11.10     oneiric
# 12.04 LTS precise
# 12.10     quantal
# 13.04     raring
# 13.10     saucy
# 14.04 LTS trusty
# 14.10     utopic

schedule { "daily":
  period => daily,
  range  => "3-5",
}

Cron {
  provider => "crontab",
}
