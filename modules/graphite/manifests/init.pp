class graphite($dbpassword = hiera('graphite::dbpassword')
) {
  # This class sets up a graphite server with the web interface
  # available via https at port 444.  It assumes that it shares the
  # TLS certificate with a Nagios installation; the graphite web
  # interface should be accessed via
  # https://nagios.ryttargardskyrkan.se:4446/ which is assumed to be
  # forwared to port 444 on the host that includes this class.
  #
  # I would rather have set up graphite so that it uses the same port
  # as the apache installation, and a different URL prefix such as
  # /graphite/, but it can only be served from / for now.  See this issue:
  #
  # https://github.com/graphite-project/graphite-web/issues/223

  include postgres
  include postgres::libpq
  include postgres::python
  include apache::wsgi_py2

  class { 'graphite::local_settings':
    dbpassword => $dbpassword,
  }

  package { ['graphite-web', 'graphite-carbon']:
    ensure => installed,
  }

  class { 'postgresql::server': }

  postgresql::server::role { 'graphite':
    password_hash => postgresql_password('graphite', $dbpassword),
  }

  postgresql::server::database { 'graphite':
    owner    => 'graphite',
    locale   => 'C',
    encoding => 'UTF8',
  }

  file { '/etc/default/graphite-carbon':
    content => "CARBON_CACHE_ENABLED=true\n",
    require => Package['graphite-carbon'],
    notify  => Service['carbon-cache'],
  }

  exec { 'carbon logrotation':
    command => '/bin/sed -i "s/ENABLE_LOGROTATION = .*/ENABLE_LOGROTATION = True/" /etc/carbon/carbon.conf',
    unless  => '/bin/grep "ENABLE_LOGROTATION = True" /etc/carbon/carbon.conf',
    require => Package['graphite-carbon'],
    notify  => Service['carbon-cache'],
  }

  file { '/etc/carbon/storage-schemas.conf':
    content => template('graphite/storage-schemas.conf.erb'),
    require => Package['graphite-carbon'],
    before  => Service['carbon-cache'],
  }

  file { '/etc/carbon/storage-aggregation.conf':
    content => template('graphite/storage-aggregation.conf.erb'),
    require => Package['graphite-carbon'],
    before  => Service['carbon-cache'],
  }

  service { 'carbon-cache':
    enable => true,
    ensure => running,
  }

  file { '/etc/apache2/sites-available/apache2-graphite.conf':
    content => template('graphite/apache2-graphite.conf.erb'),
    require => Package['apache2', 'graphite-web'],
  }

  exec { '/usr/sbin/a2ensite apache2-graphite':
    require => File['/etc/apache2/sites-available/apache2-graphite.conf'],
    creates => '/etc/apache2/sites-enabled/apache2-graphite.conf',
    notify  => Service['apache2'],
  }

  exec { '/bin/echo Listen 444 https >> /etc/apache2/ports.conf':
    unless => '/bin/grep -q 444 /etc/apache2/ports.conf',
    notify  => Service['apache2'],
  }

  @@nagios_service { "$hostname https carbon":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => 'https carbon',
    check_command       => 'check-https!444',
    target              => rg_nagios_target('https carbon'),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  nagios_command { 'graphite_perf_host':
    command_line => '/bin/mv /var/spool/nagios/graphios/host-perfdata /var/spool/nagios/graphios/host-perfdata.$TIMET$'
  }

  nagios_command { 'graphite_perf_service':
    command_line => '/bin/mv /var/spool/nagios/graphios/service-perfdata /var/spool/nagios/graphios/service-perfdata.$TIMET$'
  }

  file { '/var/spool/nagios':
    ensure => directory,
  }

  file { '/var/spool/nagios/graphios':
    ensure => directory,
  }
}
