class graphite::local_settings($dbpassword) {
  include recode

  $cfg = '/etc/graphite/local_settings.py'
  $tz = 'Europe/Stockholm'

  exec { 'graphite secret key':
    command => "/bin/sed -i \"s%^#*SECRET_KEY.*%SECRET_KEY = '`/bin/dd if=/dev/urandom bs=1 count=50  2>/dev/null |/usr/bin/recode ../base-64`'%\" ${cfg}",
    require => Package['recode'],
    unless  => "/bin/grep -q '^SECRET_KEY = ................' ${cfg}",
  }

  exec { 'graphite time zone':
    command => "/bin/sed -i \"s%^#*TIME_ZONE = .*%TIME_ZONE = '${tz}'%\" ${cfg}",
    unless  => "/bin/grep -q \"^TIME_ZONE = '${tz}'\" ${cfg}",
  }

  exec { 'graphite remote user auth':
    command => "/bin/sed -i \"s%^#*USE_REMOTE_USER_AUTHENTICATION.*%USE_REMOTE_USER_AUTHENTICATION = True%\" ${cfg}",
    unless  => "/bin/grep -q \"^USE_REMOTE_USER_AUTHENTICATION = True\" ${cfg}",
  }

  exec { 'graphite db':
    command => "/bin/sed -i \"/DATABASES/,/^}/c\\\nDATABASES = {\\\\n    'default': {\\\\n        'NAME': 'graphite',\\\\n        'ENGINE': 'django.db.backends.postgresql_psycopg2',\\\\n        'USER': 'graphite',\\\\n        'PASSWORD': '${dbpassword}',\\\\n        'HOST': '127.0.0.1',\\\\n        'PORT': ''\\\\n    }\\\\n}\n\" ${cfg}",
    unless  => "/bin/grep -q 'ENGINE.*django.db.backends.postgresql_psycopg2' ${cfg}",
  }
}

