class nis {
  case $operatingsystem {
    'Solaris': {
      $nisservice = []
    }
    default : {
      include lsb_release

      package { "nis":
        ensure => installed,
        tag    => "nisboot",
      }

      if $lsbdistrelease != '' {
        if versioncmp($lsbdistrelease, '10.04') < 0 {
          $nisservice = ['nis']
        }
        else {
          $nisservice = ['ypbind', 'ypserv', 'yppasswdd', 'ypxfrd']
        }
      }
      else {
        $nisservice = []
      }

      service { $nisservice:
        enable  => true,
        require => Package["nis"],
        tag    => "nisboot",
      }
    }
  }
}

define nis::conf($server) {
  $domain = $title

  case $operatingsystem {
    'Solaris': {
      exec { "/bin/echo $server nis.ryttargarden nis >> /etc/hosts":
        unless => "/bin/grep -q nis /etc/hosts",
        tag    => "nisboot",
      }

      file { "/etc/defaultdomain":
        content => "$domain\n",
      }
    }
    default: {

      file { "/etc/yp.conf":
        content => template("nis/conf.erb"),
        notify  => Service[$nis::nisservice],
        tag    => "nisboot",
      }

      file { "/etc/defaultdomain":
        content => "$domain\n",
        notify  => Service[$nis::nisservice],
        tag    => "nisboot",
      }

      file { "/etc/default/nis":
        source => 'puppet:///modules/nis/nis.default',
        notify => Service[$nis::nisservice],
        tag    => 'nisboot',
        owner  => 'root',
        group  => 'root',
        mode   => '0444',
      }
    }
  }

  file { "/etc/nsswitch.conf":
    source => 'puppet:///modules/nis/nsswitch.conf',
    notify => Service[$nis::nisservice],
    tag    => 'nisboot',
    owner  => 'root',
    group  => 'root',
    mode   => '0444',
  }
}

define nis::server($servertype, $nismaster="", $checkuser="nobody") {
  $domain = $title
  $clienttype = false
  
  file { "/etc/yp.conf":
    content => template("nis/conf-server.erb"),
    notify  => Service[$nis::nisservice],
    tag     => "nisboot",
  }

  file { "/etc/defaultdomain":
    content => "$domain\n",
    notify  => Service[$nis::nisservice],
    tag     => "nisboot",
  }

  file { "/etc/default/nis":
    content => template("nis/nis.erb"),
    notify  => Service[$nis::nisservice],
    tag    => "nisboot",
  }    

  file { "/etc/nsswitch.conf":
    source => "puppet:///modules/nis/nsswitch_server.conf",
    notify  => Service[$nis::nisservice],
    tag    => "nisboot",
  }

  @@nagios_service { "$hostname ypmatch":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "NIS",
    servicegroups       => "NIS",
    check_command       => "check-ypmatch!$domain!$checkuser",
    target              => rg_nagios_target("ypmatch"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}

class nis::nagios {
  nagios_command { "check-ypmatch":
    command_line => "/usr/lib/nagios/plugins/check_ypmatch -H \$HOSTADDRESS\$ -d \$ARG1\$ -k \$ARG2\$ -w 1.5 -c 8.5",
  }

  nagios_servicegroup { "NIS":
    alias => "NIS",
  }
}
