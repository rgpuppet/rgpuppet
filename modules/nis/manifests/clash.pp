class nis::clash {
  include nagios::nrpe

  $checker = '/usr/lib/nagios/plugins/check_nis_clash'

  file { $checker:
    content => template('nis/check_nis_clash.erb'),
    notify  => Service['nagios-nrpe-server'],
    mode    => '0755',
  }

  file { '/etc/nagios/nrpe.d/check_nis_clash_passwd.cfg':
    content => "command[check_nis_clash_passwd]=${checker} passwd\n",
    notify  => Service['nagios-nrpe-server'],
  }

  file { '/etc/nagios/nrpe.d/check_nis_clash_group.cfg':
    content => "command[check_nis_clash_group]=${checker} group\n",
    notify  => Service['nagios-nrpe-server'],
  }

  @@nagios_service { "$hostname check-nis-clash passwd":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => 'nis-clash passwd',
    servicegroups       => 'NIS',
    check_command       => 'check_nrpe_1arg!check_nis_clash_passwd',
    target              => rg_nagios_target('nis-clash'),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  @@nagios_service { "$hostname check-nis-clash group":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => 'nis-clash group',
    servicegroups       => 'NIS',
    check_command       => 'check_nrpe_1arg!check_nis_clash_group',
    target              => rg_nagios_target('nis-clash'),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}

