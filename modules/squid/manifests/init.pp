class squid {
  # Ubuntu 12.04 uses "squid", 14.04 "squid3".
  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    $squid = 'squid'
  }
  else {
    $squid = 'squid3'
  }

  package { $squid:
    ensure => installed,
  }

  service { $squid:
    enable => true,
  }
}
