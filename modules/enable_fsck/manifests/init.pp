class enable_fsck {
  file { '/usr/local/bin/enable_fsck':
    content => template('enable_fsck/enable_fsck.sh.erb'),
    mode    => '0755',
  }
}
