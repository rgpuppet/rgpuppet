class mediawiki {
  include apache
  include imagemagick

  package { "mediawiki":
    ensure => installed,
  }

  file { "/etc/mediawiki/LocalSettings.php":
    owner => "www-data",
    group => "www-data",
    mode => '0600',
  }
}
