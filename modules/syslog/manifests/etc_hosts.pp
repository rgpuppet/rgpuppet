define syslog::etc_hosts($service) {
  $server_fqdn = $title

  $ip = split( generate( "/usr/bin/getent", "--service=dns", "hosts",
                         "${server_fqdn}" ), ' ')

  $server_split = split( $server_fqdn, '[.]' )

  $line = "${ip[0]} ${server_fqdn} ${server_split[0]}"

  exec { "/bin/echo \"${line}\" >> /etc/hosts":
    unless => "/bin/grep -F \"${line}\" /etc/hosts",
    tag    => "bootstrap",
    notify => Service[$service],
  }
}
