class syslog($servers = hiera('syslog::servers')) {
  if rg_member($fqdn, $servers) {
    class { 'syslog::server':
      servers => $servers,
    }
  }
  else {
    class { 'syslog::client':
      servers => $servers,
    }
  }
}
