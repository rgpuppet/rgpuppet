class syslog::client($servers) {

  # Are we using syslog or rsyslog?
  $syslog = $lsbdistrelease ? {
    "8.04"  => 'syslog',
    "8.10"  => 'syslog',
    default => 'rsyslog',
  }

  case $syslog {
    'syslog': {
      $service = 'syslog'
      $syslogfile = '/etc/syslog.conf'
    }
    'rsyslog': {
      $service = 'rsyslog'
      $syslogfile = '/etc/rsyslog.d/loghost.conf'
    }
  }

  # Remove old config that logs to "loghost".
  exec { "/bin/sed -i '/loghost/d' /etc/hosts":
    onlyif => "/bin/grep -q loghost /etc/hosts",
    notify => Service[$service],
  }

  exec { "/bin/sed -i '/@loghost/d' $syslogfile":
    onlyif => "/bin/grep -q @loghost $syslogfile",
    notify => Service[$service],
  }

  service { $service:
  }

  syslog::etc_hosts { $servers:
    service => $service,
  }

  syslog::syslogfile { $servers:
    service    => $service,
    syslogfile => $syslogfile,
  }
}
