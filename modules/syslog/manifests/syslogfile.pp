define syslog::syslogfile($service, $syslogfile) {
  $server = $title

  exec { "/bin/echo '*.debug @${server}' >> $syslogfile":
    unless => "/bin/grep -q @${server} $syslogfile",
    tag    => "bootstrap",
    notify => Service[$service],
  }
}
