class syslog::server($syslogdir = '/syslog',
                     $servers) {

  include cron

  package { "syslog-ng":
    ensure => installed,
  }

  service { "syslog-ng":
    enable => true,
  }

  file { "/etc/syslog-ng/conf.d/log-archive.conf":
    content => template("syslog/log-archive.conf.erb"),
    require => Package["syslog-ng"],
    notify  => Service["syslog-ng"],
  }

  cron { 'gzip-syslog-data':
    command => '/usr/bin/find /syslog/ -type f ! -name "*.gz" -ctime +65 -exec /bin/gzip -9 "{}" ";"',
    user    => 'root',
    hour    => [2],
    minute  => 9,
  }

}
