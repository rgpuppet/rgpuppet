class ntpd::static_ip {
  file { '/var/lib/ntp/ntp.conf.dhcp':
    ensure => absent,
    notify => Service['ntp'],
  }
}
