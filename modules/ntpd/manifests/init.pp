class ntpd {
  package { "ntp":
    ensure => installed,
  }

  service { "ntp":
    enable => true,
    ensure => running,
  }

  file { "/etc/ntp.conf":
    content => template("ntpd/ntp.conf.erb"),
    notify  => Service["ntp"],
    require => Package["ntp"],
  }

  @@nagios_service { "$hostname ntp":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "NTP",
    check_command       => "check-ntp-peer",
    target              => rg_nagios_target("ntp"),
    max_check_attempts  => $hostname ? {
      "jeos-9-10-ltsp"     => 60,
      default              => 30,
    },
    contact_groups      => hiera('nagios::contact_groups'),
    icon_image          => rg_graphios('NTP.offset'),
  }
}

class ntpd::nagios {
  nagios_command { "check-ntp-peer":
    command_line => "/usr/lib/nagios/plugins/check_ntp_peer -w 0.3 -c 0.9 -j -1:120 -k -1:250 -H \$HOSTADDRESS\$",
  }
}
