class hddtemp {
  package { "hddtemp":
    ensure => installed,
  }

  if versioncmp($lsbdistrelease, "10.04") <= 0 {
    service { "hddtemp":
      enable    => true,
      ensure    => running,
      hasstatus => false,
      pattern   => "/usr/sbin/hddtemp",
    }
  } else {
    service { "hddtemp":
      enable    => true,
      ensure    => running,
    }
  }

  $cfg = "/etc/default/hddtemp"

  exec { "/bin/sed -i 's/^RUN_DAEMON=.*/RUN_DAEMON=\"true\"/' $cfg":
    unless => "/bin/grep -q '^RUN_DAEMON=\"true\"\$' $cfg",
    notify => Service["hddtemp"],
  }

  exec { "/bin/sed -i 's/^INTERFACE=.*/INTERFACE=\"0.0.0.0\"/' $cfg":
    unless => "/bin/grep -q '^INTERFACE=\"0\\.0\\.0\\.0\"\$' $cfg",
    notify => Service["hddtemp"],
  }
}

define hddtemp::check($warn, $crit, $hysteresis=1) {
  $drive=$title

  @@nagios_service { "$hostname hddtemp $drive":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "hddtemp $drive",
    servicegroups       => "hddtemp",
    check_command       => "check-hddtemp!/dev/$drive!$warn!$crit!$hysteresis",
    max_check_attempts  => 10,
    target              => rg_nagios_target("hddtemp"),
    contact_groups      => hiera('nagios::contact_groups'),
    icon_image          => rg_graphios("hddtemp${drive}.temp"),
  }
}

class hddtemp::nagios {
  nagios_command { "check-hddtemp":
    command_line => "/usr/local/nagios/libexec/check_hddtemp \$HOSTADDRESS\$ \$ARG1\$ \$ARG2\$ \$ARG3\$ \$ARG4\$",
    notify       => Service["nagios3"],
  }

  nagios_servicegroup { "hddtemp":
    alias => "hddtemp",
  }

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    file { "/etc/nagiosgrapher/ngraph.d/hddtemp.ncfg":
      content => template("hddtemp/hddtemp.ncfg.erb"),
      notify  => Service["nagiosgrapher"],
    }
  }
}
