class nut {
  package { ["nut", "nut-cgi", "nut-snmp", "nut-xml"]:
    ensure => installed,
  }
}

define nut::master($driver, $port, $listen) {
  $ups = $title

  file { "/etc/nut/nut.conf":
    content => "MODE=netserver\n",
    owner   => root,
    group   => nut,
    mode    => '0640',
  }

  file { "/etc/nut/ups.conf":
    content => template("nut/ups.conf.erb"),
    owner   => root,
    group   => nut,
    mode    => '0640',
  }

  file { "/etc/nut/upsd.conf":
    content => template("nut/upsd.conf.erb"),
    owner   => root,
    group   => nut,
    mode    => '0640',
  }

  $password = generate("/bin/cat", "/etc/upsmon.password")

  file { "/etc/nut/upsd.users":
    content => template("nut/upsd.users.erb"),
    owner   => root,
    group   => nut,
    mode    => '0640',
  }

  file { "/etc/nut/upsmon.conf":
    content => template("nut/upsmon.conf.erb"),
    owner   => root,
    group   => nut,
    mode    => '0640',
  }

  @@nagios_service { "$hostname ups-$ups":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "$ups UPS",
    check_command       => "check-ups!$ups",
    servicegroups       => "ups",
    target              => rg_nagios_target("ups-$ups"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}

class nut::nagios {
  nagios_command { "check-ups":
    command_line => "\$USER1\$/check_ups -H \$HOSTADDRESS\$ -u \$ARG1\$ -v LOADPCT -w 85 -c 95",
    notify       => Service["nagios3"],
  }

}
