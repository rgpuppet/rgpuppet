class openoffice {
  include imagemagick

  if versioncmp($lsbdistrelease, "12.04") >= 0 {
    $gcj = "gcj-jre"
    $mscore = "ttf-mscorefonts-installer"
  }
  else {
    $gcj = "java-gcj-compat"
    $mscore = "msttcorefonts"
  }

  package { ["openoffice.org", "libsane", "openclipart-openoffice.org",
             $mscore, $gcj, "pstoedit"]:
    ensure => installed,
  }
}

class openoffice::sv {
  package { ["openoffice.org-hyphenation"]:
    ensure => installed,
  }

  if versioncmp($lsbdistrelease, "10.04") >= 0 {
    package { ["openoffice.org-help-sv"]:
      ensure => installed,
    }
  }
}
