class nagios::plugins_contrib {
  package { "nagios-plugins-contrib":
    ensure => installed,
  }
}
