class nagios::check_disk_fsck_plugin {
  $plugin = '/usr/lib/nagios/plugins/check_disk_fsck'

  file { $plugin:
    content => template('nagios/check_disk_fsck_plugin.erb'),
    notify  => Service['nagios-nrpe-server'],
    mode    => '0755',
  }
}
