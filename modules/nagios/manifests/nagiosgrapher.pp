class nagios::nagiosgrapher {
  package { "nagiosgrapher":
    ensure => installed,
    notify => Service["nagiosgrapher"],
  }

  service { "nagiosgrapher":
    enable  => true,
    pattern => "/usr/bin/perl -w /usr/sbin/nagiosgrapher",
  }

  nagios::link_nagiosgrapher_cfg { 'check_disk': }
  nagios::link_nagiosgrapher_cfg { 'check_ping': }
  nagios::link_nagiosgrapher_cfg { 'check_swap': }

#  include nagios::nagiosgrapher_mailq_fix
  include nagios::nagiosgrapher_logrotate_less
}
