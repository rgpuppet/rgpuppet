class nagios::nagiosgrapher_mailq_fix {
  $cfg = "/etc/nagiosgrapher/ngraph.d/standard/check_mailq.ncfg"

  exec { "/bin/sed -i 's%\\(.*graph_perf_regex.*\\)mailq=\\(.*\\)%\\1unsent=\\2%' $cfg":
    unless => "/bin/grep -q unsent $cfg",
  }
}
