class nagios::host (
  $legend,
  $hostgroup,
  $parent = undef,
  $switch = undef,
  $switch_port = undef,
  $kvm_parent = undef,
  $kvm_host = undef,
  $ip_override = undef,
  $tsm                 = hiera("nagios::host::tsm"),
  $disks               = hiera("nagios::host::disks"),
  $disk_limits         = hiera("nagios::host::disk_limits"),
  $default_disk_limits = hiera("nagios::host::default_disk_limits"),
  $nrpe_timeout        = hiera("nagios::host::nrpe_timeout"),
  $swap                = hiera("nagios::host::swap"),
  $check_time          = hiera("nagios::host::check_time"),
  $monitor_apt         = hiera("nagios::host::monitor_apt"),
  $contact_groups      = hiera("nagios::contact_groups"),
  $load_warn           = hiera('nagios::host::load_warn'),
  $load_crit           = hiera('nagios::host::load_crit'),
  ) {

  if $ip_override {
    $ipaddress_real = $ip_override
  }
  else {
    $ipaddress_real = $ipaddress
  }

  nagios::disk { $disks:
    nrpe_timeout         => $nrpe_timeout,
    disk_limits          => $disk_limits,
    default_disk_limits  => $default_disk_limits,
  }

  if $tsm == true {
    tsm::disk_backup { $disks:
      nrpe_timeout => $nrpe_timeout,
    }
  }
  elsif $tsm {
    tsm::disk_backup { $tsm:
      nrpe_timeout => $nrpe_timeout,
    }
  }

  if $swap {
    include nagios::swap
  }

  if $check_time {
    include nagios::check_time
  }

  if $monitor_apt {
    include apt::monitor_apt
  }

  if $kvm_parent {
    $hostgroups = "${hostgroup},kvm-${kvm_parent}"
  }
  elsif $kvm_host {
    $hostgroups = "${hostgroup},kvm-${hostname}"

    @@nagios_hostgroup { "kvm-${hostname}":
      alias => "All nodes running on ${hostname}"
    }
  }
  else {
    $hostgroups = $hostgroup
  }

  @@nagios_host { $hostname:
    use            => 'rg-host',
    alias          => "${hostname} - ${legend} (${lsbdistdescription})",
    address        => $ipaddress_real,
    hostgroups     => $hostgroups,
    parents        => $parent,
    contact_groups => $contact_groups,
  }

  @@nagios_service { "${hostname} ping":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => "PING",
    servicegroups       => 'ping',
    check_command       => "check_ping!200.0,20%!500.0,60%",
    target              => rg_nagios_target("ping"),
    contact_groups      => $contact_groups,
    icon_image          => rg_graphios('PING.rta'),
  }

  @@nagios_service { "$hostname load":
    use                 => "nrpe-service",
    host_name           => $hostname,
    service_description => "LOAD",
    check_command       => "check_nrpe_1arg!check_load",
    target              => rg_nagios_target("load"),
    contact_groups      => $contact_groups,
    icon_image          => rg_graphios(
      'LOAD.load{15,5,1}',
      '&areaMode=stacked&colorList=red,orange,yellow&yMin=0',
      '&bgcolor=black'),
  }

  # Uses $load_warn and $load_crit.
  file { '/etc/nagios/nrpe.d/check_load.cfg':
    content => template('nagios/nrpe_check_load.erb'),
    notify  => Service['nagios-nrpe-server'],
  }

  # Check all disks in a single check command. This should be
  # redundant, but can be valuable in case we forgot to add a
  # partition to nagios::host::disks.

  file { '/etc/nagios/nrpe.d/s_nagios.cfg':
    content => template('nagios/nrpe_check_disks.erb'),
    notify  => Service['nagios-nrpe-server'],
  }

  @@nagios_service { "${hostname} disk":
    use                 => 'nrpe-disk',
    host_name           => $hostname,
    service_description => 'disk',
    check_command       => 'check_nrpe_1arg!check_disks',
    target              => rg_nagios_target('disk'),
    contact_groups      => $contact_groups,
  }


  if $switch {
    s_nagios::switchport { "$switch switchport $switch_port":
      switch           => $switch,
      switch_port      => $switch_port,
      hostname         => $hostname,
      use_store_config => true,
      contact_groups   => $contact_groups,
    }
  }
}
