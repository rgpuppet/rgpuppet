class nagios::send_nsca {
  package { 'nsca-client':
    ensure => installed,
  }
}
