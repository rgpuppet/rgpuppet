define nagios::check_disk_fsck() {

  $mountpoint = $title

  include nagios::nrpe
  include nagios::check_disk_fsck_plugin
  include sudo

  $diskname = $mountpoint ? {
    "/"     => "root",
    default => regsubst(regsubst($mountpoint, "^/", ""),
                        '/', '_', 'G'),
  }

  file { "/etc/nagios/nrpe.d/check_disk_fsck_$diskname.cfg":
    content => "command[check_disk_fsck_$diskname]=/usr/bin/sudo $nagios::check_disk_fsck_plugin::plugin $mountpoint\n",
    notify  => Service["nagios-nrpe-server"],
  }

  exec { "/bin/echo 'nagios ALL = (ALL) NOPASSWD: ${nagios::check_disk_fsck_plugin::plugin} ${mountpoint}' >> /etc/sudoers":
    unless  => "/bin/grep -q '${nagios::check_disk_fsck_plugin::plugin} ${mountpoint}\$' /etc/sudoers",
    require => Package['sudo'],
  }

  @@nagios_service { "$hostname fsck $diskname":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "disk fsck $mountpoint",
    servicegroups       => 'fsck',
    check_command       => "check-nrpe-timeout!20!check_disk_fsck_$diskname",
    target              => rg_nagios_target("disk-fsck-${diskname}"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}
