class nagios::nagiosgrapher_logrotate_less {
  $cfg = "/etc/logrotate.d/nagiosgrapher"

  exec { "/bin/sed -i 's/rotate 52/rotate 10/' $cfg":
    unless => "/bin/grep -q 'rotate 10' $cfg",
  }
}
