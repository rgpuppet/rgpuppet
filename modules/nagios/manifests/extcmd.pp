class nagios::extcmd($puppetmaster = hiera('nagios::extcmd::puppetmaster')) {
  group { 'nagiocmd':
    ensure => present,
    system => true,
  }

  if $puppetmaster {
    exec { "addgroup puppet nagiocmd":
      command => "/usr/sbin/addgroup puppet nagiocmd",
      unless  => "/bin/grep -q '^nagiocmd:.*[:,]puppet\\(,\\|\$\\)' /etc/group",
      require => [Group['nagiocmd'], Package[$puppet::master::pkg]],
      notify  => Service[$puppet::master::srv],
    }
  }

  exec { "addgroup www-data nagiocmd":
    command => "/usr/sbin/addgroup www-data nagiocmd",
    unless  => "/bin/grep -q '^nagiocmd:.*[:,]www-data\\(,\\|\$\\)' /etc/group",
    require => [Group['nagiocmd'], Package['apache2']],
    notify  => Service['apache2'],
  }

  # Enable external commands, as per instructions in
  # /usr/share/doc/nagios3/README.Debian.  Add the --force option, so
  # that this can run multiple times in case the second
  # dpkg-statoverride temporarily fails.
  # Ubuntu 10.04 has dpkg-statoverride in /usr/sbin; Ubuntu 14.04 in
  # /usr/bin.
  exec { 'enable-nagios-commands':
    path    => '/usr/bin:/usr/sbin:/bin:/sbin',
    command => '/etc/init.d/nagios3 stop &&
    dpkg-statoverride --force --update --add nagios nagiocmd 2710 /var/lib/nagios3/rw &&
    dpkg-statoverride --force --update --add nagios nagios 751 /var/lib/nagios3 &&
    /etc/init.d/nagios3 start',
    unless  => '/usr/bin/test `/usr/bin/stat -c %a /var/lib/nagios3` = 751 && /usr/bin/test `/usr/bin/stat -c %G /var/lib/nagios3/rw` = nagiocmd',
    require => Service['nagios3'],
  }
}
