define nagios::link_nagiosgrapher_cfg {
  $srcdir = '/usr/share/nagiosgrapher/debian/cfg/ngraph.d/standard'
  $dstdir = '/etc/nagiosgrapher/ngraph.d/standard'

  $cfg = "${title}.ncfg"

  exec { "/bin/ln -s ${srcdir}/${cfg} ${dstdir}/${cfg}":
    unless => "/usr/bin/test -f ${dstdir}/${cfg}",
    onlyif => "/usr/bin/test -f ${srcdir}/${cfg}",
  }
}
