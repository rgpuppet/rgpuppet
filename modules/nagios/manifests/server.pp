class nagios::server(
  $cgi_users   = hiera('nagios::server::cgi_users'),
  $admin_email = hiera('nagios::server::admin_email'),
  $admin_pager = hiera('nagios::server::admin_pager'),
  $hostgroups  = hiera('nagios::server::hostgroups')
  ) {

  include nagios::nsca

  nagios_host { 'rg-host':
    use            => 'generic-host',
    contact_groups => 'linux-admins',
    register       => '0',
  }

  nagios_service { 'rg-service':
    use                => 'generic-service',
    max_check_attempts => '3',
    contact_groups     => 'linux-admins',
    register           => '0',
  }

  nagios::hostgroup { $hostgroups: }

  nagios_servicegroup { 'ping':
    alias => 'ping',
  }

  nagios_servicegroup { 'disk':
    alias => 'disk',
  }

  nagios_servicegroup { 'swap':
    alias => 'swap',
  }

  nagios_servicegroup { 'time':
    alias => 'time',
  }

  nagios_servicegroup { 'fsck':
    alias => 'fsck',
  }

  nagios_servicegroup { 'apt':
    alias => 'apt',
  }

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    include nagios::nagiosgrapher
    $use_graphite = false
  }
  else {
    include nagios::graphite
    include nagios::dot
    $use_graphite = true
  }

  package { ["nagios3", "nagios-images", "nagios-plugins", "nagios3-doc",
  	     "nagios-nrpe-plugin"]:
    ensure => installed,
  }

  file { '/etc/nagios3/apache2.conf':
    content => template('nagios/apache2.conf.erb'),
    require => Package['nagios3'],
    notify  => Service['nagios3', 'apache2'],
  }

  # The init script in Debian 6.0.5 is broken.  Apply the up-stream
  # fix found here:
  # http://anonscm.debian.org/gitweb/?p=pkg-nagios/pkg-nagios3.git;a=commitdiff;h=9057336823a854a50c2f91bb8b089d78046bc6b7
  exec { 'fix-nagios-init-script':
    require => Package['nagios3'],
    command => '/bin/sed -i -e "/^status () {/,/}/d" /etc/init.d/nagios3',
    onlyif  => '/usr/bin/test `/bin/grep -c ^status /etc/init.d/nagios3` -eq 2',
    before  => Service['nagios3'],
  }

  service { "nagios3":
    enable  => true,
  }

  file { '/etc/default/nagios3':
    require => Package['nagios3'],
    notify  => Service['nagios3'],
    content => template('nagios/nagios-defaults.sh')
  }

  include nagios::extcmd
  include nagios::etc_nagios

  $nagios_files = [ '/etc/nagios/nagios_command.cfg',
                    '/etc/nagios/nagios_host.cfg',
                    '/etc/nagios/nagios_service.cfg',
                    '/etc/nagios/nagios_servicegroup.cfg',
                    '/etc/nagios/nagios_hostgroup.cfg',
                    ]

  file { $nagios_files:
    mode   => '0644',
    ensure => present,
    notify => Service['nagios3'],
  }

  Nagios_host <<| |>>
  Nagios_hostgroup <<| |>>
  Nagios_service <<| |>>
  Nagios_servicegroup <<| |>>
  Nagios_command <<| |>>

  file { "/etc/nagios3/puppet-services.d":
    ensure  => directory,
    mode    => '0644',
    recurse => true,
    before  => Service['nagios3'],
  }

  Service["apache2"] -> Service["nagios3"]

#  file { "/etc/apache2/nagios3.conf":
#    ensure => link,
#    target => "/etc/nagios3/apache2.conf",
#  }

#  file { "/etc/nagios3/conf.d/contacts.cfg":
#    content => template("nagios/contacts.cfg.erb"),
#  }

  # Remove default files that we don't want.
  file { "/etc/nagios3/conf.d/host-gateway_nagios3.cfg":
    ensure => absent,
  }

  file { "/etc/nagios3/conf.d/contacts_nagios2.cfg":
    ensure => absent,
  }

  file { "/etc/nagios3/conf.d/hostgroups_nagios2.cfg":
    ensure => absent,
  }

  file { "/etc/nagios3/conf.d/services_nagios2.cfg":
    ensure => absent,
  }

  file { "/etc/nagios3/conf.d/extinfo_nagios2.cfg":
    ensure => absent,
  }

  file { "/etc/nagios3/conf.d/localhost_nagios2.cfg":
    ensure => absent,
  }

  # This template uses $use_graphite.
  file { "/etc/nagios3/nagios.cfg":
    content => template("nagios/nagios.cfg.erb"),
    notify => Service["nagios3"],
  }

  file { "/etc/nagios3/cgi.cfg":
    content => template("nagios/cgi.cfg.erb"),
  }

  nagios_command { "check-nrpe-timeout":
    command_line => "/usr/lib/nagios/plugins/check_nrpe -H \$HOSTADDRESS\$ -t \$ARG1\$ -c \$ARG2\$"
  }
}
