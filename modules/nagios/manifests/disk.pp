define nagios::disk (
  $nrpe_timeout,
  $disk_limits,
  $default_disk_limits,
  ) {

  include nagios::nrpe

  $mountpoint = $title

  $diskname = $mountpoint ? {
    "/"     => "root",
    default => regsubst(regsubst($mountpoint, "^/", ""),
                        '/', '_', 'G'),
  }

  $graphiosname = regsubst($mountpoint, '/', '_', 'G')

  $limits = $disk_limits[$mountpoint] ? {
    ""      => $default_disk_limits,
    undef   => $default_disk_limits,
    default => $disk_limits[$mountpoint],
  }

  $warn_real = $limits['warn'] ? {
    ""      => $default_disk_limits['warn'],
    undef   => $default_disk_limits['warn'],
    default => $limits['warn'],
  }
  $crit_real = $limits['crit'] ? {
    ""      => $default_disk_limits['crit'],
    undef   => $default_disk_limits['crit'],
    default => $limits['crit'],
  }
  $iwarn_real = $limits['iwarn'] ? {
    ""      => $default_disk_limits['iwarn'],
    undef   => $default_disk_limits['iwarn'],
    default => $limits['iwarn'],
  }
  $icrit_real = $limits['icrit'] ? {
    ""      => $default_disk_limits['icrit'],
    undef   => $default_disk_limits['icrit'],
    default => $limits['icrit'],
  }

  file { "/etc/nagios/nrpe.d/check_disk_$diskname.cfg":
    content => "command[check_disk_$diskname]=/usr/lib/nagios/plugins/check_disk -E -w ${warn_real}% -c ${crit_real}% -W ${iwarn_real}% -K ${icrit_real}% -p $mountpoint\n",
    notify  => Service["nagios-nrpe-server"],
  }

  @@nagios_service { "$hostname disk $diskname":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "disk $mountpoint",
    servicegroups       => 'disk',
    check_command       => "check-nrpe-timeout!$nrpe_timeout!check_disk_$diskname",
    target              => rg_nagios_target("disk-${diskname}"),
    contact_groups      => hiera('nagios::contact_groups'),
    icon_image          => rg_graphios(
      "disk${graphiosname}.${graphiosname}",
      "&yMin=0"),
  }
}
