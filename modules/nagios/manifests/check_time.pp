class nagios::check_time($host = hiera('nagios::check_time::host')) {
  include nagios::nrpe

  @@nagios_service { "${hostname} time":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => 'time',
    servicegroups       => 'time',
    check_command       => 'check_nrpe_1arg!check_time',
    target              => rg_nagios_target('time'),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  file { "/etc/nagios/nrpe.d/check_time.cfg":
    content => "command[check_time]=/usr/lib/nagios/plugins/check_ntp_time -H ${host} -w 0.5 -c 2.0\n",
    notify  => Service['nagios-nrpe-server'],
  }
}
