class nagios::plugins_standard {
  package { "nagios-plugins-standard":
    ensure => installed,
  }
}
