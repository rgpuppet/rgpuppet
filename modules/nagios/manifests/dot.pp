class nagios::dot {
  file { '/usr/share/nagios3/htdocs/images/logos/dot.png':
    source => 'puppet:///modules/nagios/dot.png',
  }
}
