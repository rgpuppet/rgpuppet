class nagios::check_ifstatus {
  nagios_command { 'check-snmp-ifstatus':
    command_line => "/usr/lib/nagios/plugins/check_snmp_ifstatus -6 -H \$ARG1\$ -v 2 -i \"\$ARG2\$\"",
    notify       => Service['nagios3'],
  }

  nagios_command { 'check-snmp-ifstatus-32bit':
    command_line => "/usr/lib/nagios/plugins/check_snmp_ifstatus -H \$ARG1\$ -v 2 -i \"\$ARG2\$\"",
    notify       => Service['nagios3'],
  }

  nagios_command { 'check-snmp-ifstatus-link':
    command_line => "/usr/lib/nagios/plugins/check_snmp_ifstatus -l -H \$ARG1\$ -v 2 -i \"\$ARG2\$\"",
    notify       => Service['nagios3'],
  }

  file { '/usr/lib/nagios/plugins/check_snmp_ifstatus':
    content => template('nagios/check_snmp_ifstatus.pl.erb'),
    notify  => Service['nagios3'],
    mode    => '0755',
  }
}
