class nagios::nsca {
  package { 'nsca':
    ensure => installed,
  }

  service { 'nsca':
    enable => true,
    ensure => running,
  }
}
