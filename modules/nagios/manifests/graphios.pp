class nagios::graphios {
  # Manually do this:
  #
  #    git clone https://github.com/shawn-sterling/graphios.git
  #    cd graphios
  #    python setup.py build
  #    sudo python setup.py install
  #    sudo cp init/debian/graphios /etc/init.d/
  #    sudo cp init/debian/graphios.conf /etc/init
  #    sudo chmod 755 /etc/init.d/graphios

  include python::setuptools

  $cfg = '/etc/graphios/graphios.cfg'

  exec { "/bin/sed -i 's/^\\(use_service_desc\\) *=.*\$/\\1 = True/' ${cfg}":
    unless => "/bin/grep -q '^use_service_desc = True$' ${cfg}",
  }

  exec { "/bin/sed -i 's/^\\(enable_carbon\\) *=.*\$/\\1 = True/' ${cfg}":
    unless => "/bin/grep -q '^enable_carbon = True$' ${cfg}",
  }

  exec { "/bin/sed -i 's/^\\(debug\\) *=.*\$/\\1 = False/' ${cfg}":
    unless => "/bin/grep -q '^debug = False$' ${cfg}",
  }

  service { 'graphios':
    enable => true,
    ensure => running,
  }

  file { '/usr/local/nagios/var':
    ensure => directory,
    mode   => '2777',
  }

  file { '/etc/logrotate.d/graphios':
    content => template('nagios/graphios.logrotate.erb'),
  }
}
