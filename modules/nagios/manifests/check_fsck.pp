class nagios::check_fsck($disks = hiera('nagios::host::disks')) {
  nagios::check_disk_fsck { $disks:
  }
}
