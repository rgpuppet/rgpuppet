define nagios::hostgroup {
 $hostgroup = regsubst($name, '([^:]*):.*', '\1')
 $alias = regsubst($name, '[^:]*:(.*)', '\1')

  nagios_hostgroup { $hostgroup:
    alias => $alias,
  }
}
