define nagios::check_file_age($warning, $critical, $service_description) {
  include nagios::nrpe

  $file = $title
  $safe_file = shellquote(regsubst($file, '/', '-', 'G'))

  file { "/etc/nagios/nrpe.d/check_file_age_${safe_file}.cfg":
    content => "command[check_file_age_${safe_file}]=/usr/lib/nagios/plugins/check_file_age -w ${warning} -c ${critical} -f ${file}\n",
    notify  => Service['nagios-nrpe-server'],
  }

  @@nagios_service { "$hostname file_age $safe_file":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => $service_description,
    check_command       => "check_nrpe_1arg!check_file_age_${safe_file}",
    target              => rg_nagios_target("file_age-${safe_file}"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}
