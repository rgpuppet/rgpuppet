class nagios::nrpe ($allowed_hosts = hiera('nagios::nrpe::allowed_hosts')) {
  package { ["nagios-plugins-basic", "nagios-nrpe-server"]:
    ensure => installed,
  }

  service { "nagios-nrpe-server":
    enable     => true,
    pattern    => "/usr/sbin/nrpe",
    hasstatus  => false,
  }

  include nagios::etc_nagios

  Package["nagios-nrpe-server"] -> File["/etc/nagios"]

  file { "/etc/nagios/nrpe.cfg":
    content => template("nagios/nrpe.cfg.erb"),
    notify  => Service["nagios-nrpe-server"],
    require => Package['nagios-nrpe-server'],
  }

  file { '/etc/default/nagios-nrpe-server':
    content => template('nagios/nrpe-defaults.sh'),
    notify  => Service['nagios-nrpe-server'],
    require => Package['nagios-nrpe-server'],
  }

  file { "/etc/nagios/nrpe.d":
    ensure => directory,
  }
}
