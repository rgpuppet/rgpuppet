class nagios::swap (
  $crit = hiera('nagios::swap::crit'),
  $warn = hiera('nagios::swap::warn'),
  ) {

  include nagios::nrpe

  file { "/etc/nagios/nrpe.d/check_swap.cfg":
    content => "command[check_swap]=/usr/lib/nagios/plugins/check_swap -w ${warn}% -c ${crit}%\n",
    notify  => Service["nagios-nrpe-server"],
  }

  @@nagios_service { "${hostname} swap":
    use                 => "rg-service",
    host_name           => $hostname,
    service_description => "swap",
    servicegroups       => 'swap',
    check_command       => "check_nrpe_1arg!check_swap",
    target              => rg_nagios_target('swap'),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}
