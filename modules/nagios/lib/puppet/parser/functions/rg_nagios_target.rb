module Puppet::Parser::Functions
  newfunction(:rg_nagios_target, :type => :rvalue) do |args|
    if args.empty?
      raise Puppet::ParseError, "missing argument to rg_nagios_target"
    end
    return "/etc/nagios3/puppet-services.d/" + args[0] + "-" +
      lookupvar('hostname').to_s + ".cfg"
  end
end
