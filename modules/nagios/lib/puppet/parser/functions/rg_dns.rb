require 'resolv'

module Puppet::Parser::Functions
  newfunction(:rg_dns, :type => :rvalue) do |args|
    if args.empty?
      raise Puppet::ParseError, "missing argument to rg_dns"
    end

    if args.size > 1
      raise Puppet::ParseError, "too many arguments to rg_dns"
    end

    Resolv::DNS.open() do |resolver|
      return resolver.getaddress(args[0])
    end
  end
end
