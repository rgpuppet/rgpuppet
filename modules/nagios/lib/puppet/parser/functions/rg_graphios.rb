module Puppet::Parser::Functions
  newfunction(:rg_graphios, :type => :rvalue) do |args|
    if args.empty?
      raise Puppet::ParseError, "missing argument to rg_graphios"
    end

    # LOAD.load{15,5,1}
    target = "nagios.srv." + lookupvar('hostname').to_s + "." + args[0]

    # &areaMode=stacked&colorList=red,orange,yellow
    common = ""
    if args.size > 1
      common = args[1]
    end

    # &bgcolor=black
    small_only = ""
    if args.size > 2
      small_only = args[2]
    end

    base = "https://nagios.ryttargardskyrkan.se:4446/render?"
    large = "&from=-1days&height=600&width=800"
    small = "&from=-1hours&height=40&width=40&graphOnly=true"

    return "dot.png' alt=\"\" border=\"0\"></a><a target=\"_blank\" href=\"" +
      base + "target=" + target + large + common +
      "\"><img src='" +
      base + "target=" + target + small + common + small_only
  end
end
