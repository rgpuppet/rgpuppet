# This file is managed by Puppet.  Source:
# modules/nagios/templates/nrpe-defaults.sh inside the
# git@piltaj:/var/git/rgpuppet.git repo.

# defaults file for nagios-nrpe-server
# (this file is a /bin/sh compatible fragment)

# DAEMON_OPTS are any extra cmdline parameters you'd like to
#             pass along to the nrpe daemon
#DAEMON_OPTS="--no-ssl"

# NICENESS is if you want to run the server at a different nice() priority
#NICENESS=5

# INETD is if you want to run the server via inetd (default=0, run as daemon)
#INETD=0

# If somebody runs "puppet agent --test" manually and that causes nrpe
# to restart, nrpe might pick up a locale that cause Nagios tests to
# fail.

LANG=C
LANGUAGE=C
LC_CTYPE=C
LC_NUMERIC=C
LC_TIME=C
LC_COLLATE=C
LC_MONETARY=C
LC_MESSAGES=C
LC_PAPER=C
LC_NAME=C
LC_ADDRESS=C
LC_TELEPHONE=C
LC_MEASUREMENT=C
LC_IDENTIFICATION=C
LC_ALL=C

export LANG
export LANGUAGE
export LC_CTYPE
export LC_NUMERIC
export LC_TIME
export LC_COLLATE
export LC_MONETARY
export LC_MESSAGES
export LC_PAPER
export LC_NAME
export LC_ADDRESS
export LC_TELEPHONE
export LC_MEASUREMENT
export LC_IDENTIFICATION
export LC_ALL
