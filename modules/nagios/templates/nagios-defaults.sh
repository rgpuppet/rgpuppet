# /etc/default/nagios3
# Source: modules/nagios/templates/nagios-defaults.sh inside
# git@piltaj:/var/git/rgpuppet.git.

# location of the nagios configuration file
NAGIOSCFG="/etc/nagios3/nagios.cfg"

# location of the CGI configuration file
CGICFG="/etc/nagios3/cgi.cfg"

# nicelevel to run nagios daemon with
NICENESS=5

# if you use pam_tmpdir, you need to explicitly set TMPDIR:
#TMPDIR=/tmp

# If somebody runs "puppet agent --test" manually and that causes
# nagios3 to restart, nagios3 might pick up a locale that cause Nagios
# tests to fail.

LANG=C
LANGUAGE=C
LC_CTYPE=C
LC_NUMERIC=C
LC_TIME=C
LC_COLLATE=C
LC_MONETARY=C
LC_MESSAGES=C
LC_PAPER=C
LC_NAME=C
LC_ADDRESS=C
LC_TELEPHONE=C
LC_MEASUREMENT=C
LC_IDENTIFICATION=C
LC_ALL=C

export LANG
export LANGUAGE
export LC_CTYPE
export LC_NUMERIC
export LC_TIME
export LC_COLLATE
export LC_MONETARY
export LC_MESSAGES
export LC_PAPER
export LC_NAME
export LC_ADDRESS
export LC_TELEPHONE
export LC_MEASUREMENT
export LC_IDENTIFICATION
export LC_ALL
