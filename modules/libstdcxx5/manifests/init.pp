class libstdcxx5 {
  package { "gcc-3.3-base":
    ensure => installed,
  }  

  package { "libstdc++5":
    ensure  => installed,
    require => Package["gcc-3.3-base"],
  }  
}
