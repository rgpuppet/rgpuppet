class arpwatch {
  package { "arpwatch":
    ensure => installed,
  }

  file { "/usr/local/bin/switch-dump-arp":
    content => template("arpwatch/switch-dump-arp.erb"),
    mode    => '0755',
  }
}
