class caviar_dreams {
  file { "/usr/share/fonts/truetype/caviardreams/":
    source  => "puppet:///modules/caviar_dreams/",
    recurse => "true",
    notify  => Exec["activate_caviar_dreams"],
    owner   => 'root',
    group   => 'root',
  }

  exec { "activate_caviar_dreams":
    command => "/bin/sh -c 'cd /usr/share/fonts/truetype/caviardreams && /usr/bin/mkfontdir && /usr/bin/fc-cache'",
    refreshonly => true,
  }
}
