class ssh::forbid_password {
  $file = '/etc/ssh/sshd_config'

  exec { "/bin/sed -i 's/^PasswordAuthentication.*/PasswordAuthentication no/' ${file}":
    unless => "/bin/grep '^PasswordAuthentication[ \t][ \t]*no\$' ${file}",
  }

  exec { "/bin/echo PasswordAuthentication no >> ${file}":
    unless => "/bin/grep '^PasswordAuthentication' ${file}",
  }
}
