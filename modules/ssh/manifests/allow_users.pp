define ssh::allow_users() {
  $users = $title
  $file = "/etc/ssh/sshd_config"

  exec { "/bin/sed -i 's/^AllowUsers.*/AllowUsers $users/' $file":
    unless => "/bin/grep '^AllowUsers[ \t][ \t]*$users\$' $file",
    notify  => Service["ssh"],
  }

  exec { "/bin/echo AllowUsers $users >> $file":
    unless => "/bin/grep '^AllowUsers' $file",
    notify  => Service["ssh"],
  }
}
