class ssh::server {
  $sshd = $operatingsystem ? {
    "Solaris" => "sunwsshdu",
    default => "openssh-server",
  }

  package { ["xauth", $sshd]:
    ensure => installed,
  }

  service { "ssh":
    enable => true,
    ensure => running,
  }

  @@sshkey { $hostname:
    type => rsa,
    key => $sshrsakey
  }

  Sshkey <<| |>>

  file { "/etc/ssh/ssh_known_hosts":
    mode  => '0644',
    owner => root,
  }

  @@nagios_service { "$hostname ssh":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "ssh",
    check_command       => "check_ssh_4",
    target              => rg_nagios_target("ssh"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}
