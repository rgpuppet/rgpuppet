class ssh::permit_root {
  $file = "/etc/ssh/sshd_config"

  exec { "/bin/sed -i 's/^PermitRootLogin.*/PermitRootLogin yes/' $file":
    unless => "/bin/grep '^PermitRootLogin[ \t][ \t]*yes\$' $file",
  }

  exec { "/bin/echo PermitRootLogin yes >> $file":
    unless => "/bin/grep '^PermitRootLogin' $file",
  }
}
