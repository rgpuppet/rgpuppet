class ssh::forbid_root {
  $file = '/etc/ssh/sshd_config'

  exec { "/bin/sed -i 's/^PermitRootLogin.*/PermitRootLogin no/' ${file}":
    unless => "/bin/grep '^PermitRootLogin[ \t][ \t]*no\$' ${file}",
  }

  exec { "/bin/echo PermitRootLogin no >> ${file}":
    unless => "/bin/grep '^PermitRootLogin' ${file}",
  }
}
