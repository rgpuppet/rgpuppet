class rwho($check_rwho = hiera('rwho::check_rwho')) {
  package { "rwho":
    ensure => installed,
  }

  service { "rwhod":
    enable  => true,
    require => Package["rwho"],
  }

  $rwho_ensure = $check_rwho ? {
    true  => 'present',
    false => 'absent',
  }

  @@nagios_service { "$hostname rwho":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "rwho",
    check_command       => "check-file-age-2!/var/spool/rwho/whod.$hostname!240!600",
    target              => rg_nagios_target("rwho"),
    contact_groups      => hiera('nagios::contact_groups'),
    ensure              => $rwho_ensure,
  }
}

class rwho::nagios {
  nagios_command { "check-file-age-2":
    command_line => "/usr/lib/nagios/plugins/check_file_age -f '\$ARG1\$' -w '\$ARG2\$' -c '\$ARG3\$'",
    notify       => Service["nagios3"],
  }
}

define rwho::options {
  $options = $title
  file { "/etc/default/rwhod":
    content => template("rwho/rwhod.erb"),
    require => Package["rwho"],
    notify  => Service["rwhod"],
  }
}
