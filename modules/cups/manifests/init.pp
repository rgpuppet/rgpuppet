class cups {
  package { "cups":
    name => $lsbdistcodename ? { 
      hardy => "cupsys",
      default => "cups",
    },
    ensure => installed,
  }

  service { "cups":
    name => $lsbdistcodename ? { 
      hardy => "cupsys",
      default => "cups",
    },
    enable => true,
  }
}

define cups::client($loglvl = "warning") {
  $server = $title

  package { "cups-client":
    ensure => installed,
  }

  file { "/etc/cups/cupsd.conf":
    content => template("cups/cupsd.conf.erb"),
    notify  => Service["cups"],
  }
  file { "/etc/cups/client.conf":
    content => "ServerName $server\n",
    notify  => Service["cups"],
  }
}
