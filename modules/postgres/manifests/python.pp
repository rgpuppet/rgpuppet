class postgres::python {
  package { 'python-psycopg2':
    ensure => installed,
  }
}
