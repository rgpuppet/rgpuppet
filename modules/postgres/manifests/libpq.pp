class postgres::libpq {
  package { 'libpq-dev':
    ensure => installed,
  }
}
