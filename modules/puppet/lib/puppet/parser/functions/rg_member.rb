module Puppet::Parser::Functions
  newfunction(:rg_member, :type => :rvalue, :doc => <<-EOS
Returns true if OBJECT is part of ARRAY, false otherwise.
    EOS
  ) do |arguments|

    if (arguments.size != 2) then
      raise(Puppet::ParseError,
            "rg_member(object, array): Got #{arguments.size} args instead of 2")
    end

    obj = arguments[0]
    ary = arguments[1]
    ary.include?(obj)
  end
end
