require 'puppet'

Puppet::Reports.register_report(:nagiosupdate_nsca) do

  desc <<-DESC
  Supply passive Nagios service checks for the Puppet run to the NSCA server.
DESC

  configfile = File.join([File.dirname(Puppet.settings[:config]),
                          "nagiosupdate_nsca.yaml"])
  if (!File.exist?(configfile))
    raise(Puppet::ParseError,
          "nagiosupdate_nsca report config file #{configfile} not readable#")
  end
  CONFIG = YAML.load_file(configfile)

  def process
    host = self.host.split(".")[0]
    prefix = host + "\t"

    report1 = prefix + self.check_status() + "\n"
    report2 = prefix + self.check_environment() + "\n"

    CONFIG["nscaservers"].each do |nsca|
      IO.popen("/usr/sbin/send_nsca -H " + nsca, "w") do |f|
        f.write(report1)
      end

      IO.popen("/usr/sbin/send_nsca -H " + nsca, "w") do |f|
        f.write(report2)
      end
    end
  end

  def check_status
    if self.status == "unchanged"
      rc = "0"
    elsif self.status == "changed"
      rc = "1"
    else
      rc = "2"
    end

    return "puppet-status\t#{rc}\tstatus: #{self.status}"
  end

  def check_environment
    if self.environment == "production"
      rc = "0"
    else
      rc = "1"
    end

    return "puppet-env\t#{rc}\tlast environment: #{self.environment}"
  end
end
