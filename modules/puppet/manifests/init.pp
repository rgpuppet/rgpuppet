class puppet ($puppetmaster = hiera('puppet::puppetmaster')) {
  class { "puppet::impl::client":
    is_master    => false,
    puppetmaster => $puppetmaster,
  }
}
