class puppet::nagios {
 if hiera('puppet_reports_to_nagios') {
   nagios_command { 'no-puppet-report':
     command_line => '/usr/lib/nagios/plugins/check_dummy 2 "No recent puppet reports received"',
     notify       => Service['nagios3'],
   }

   nagios_command { 'no-puppet-env':
     command_line => '/usr/lib/nagios/plugins/check_dummy 3 "Active check has run. Data lost."',
     notify       => Service['nagios3'],
   }
 }

  nagios_servicegroup { 'puppet':
    alias => 'puppet',
  }
}
