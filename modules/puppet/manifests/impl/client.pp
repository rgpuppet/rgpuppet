class puppet::impl::client ($is_master, $puppetmaster) {
  include puppet::apt

  package { ['puppet', 'facter']:
    ensure  => latest,
    require => Exec['update-apt'],
  }

  file { "/etc/default/puppet":
    content => template("puppet/default.erb"),
    mode    => '0444',
    require => Package['puppet'],
    notify  => Service['puppet'],
  }

  $puppet_reports_to_nagios = hiera('puppet_reports_to_nagios')

  file { "puppet.conf":
    path    => "/etc/puppet/puppet.conf",
    content => template("puppet/puppet.conf.erb"),
    require => Package["puppet"],
    notify  => Service["puppet"],
  }

  service { "puppet":
    ensure => running,
    enable => true,
  }

  if $puppet_reports_to_nagios {
    $report_ensure = 'present'
  }
  else {
    $report_ensure = 'absent'
  }

  @@nagios_service { "${hostname} puppet-status":
    use                    => 'rg-service',
    host_name              => $hostname,
    service_description    => 'puppet-status',
    servicegroups          => 'puppet',
    active_checks_enabled  => '0',
    passive_checks_enabled => '1',
    check_freshness        => '1',
    freshness_threshold    => '2000',
    check_command          => 'no-puppet-report',
    target                 => rg_nagios_target('puppet-status'),
    ensure                 => $report_ensure,
    contact_groups         => hiera('nagios::contact_groups'),
  }

  @@nagios_service { "${hostname} puppet-env":
    use                    => 'rg-service',
    host_name              => $hostname,
    service_description    => 'puppet-env',
    servicegroups          => 'puppet',
    active_checks_enabled  => '0',
    passive_checks_enabled => '1',
    check_freshness        => '0',
    check_command          => 'no-puppet-env',
    target                 => rg_nagios_target('puppet-env'),
    ensure                 => $report_ensure,
    contact_groups         => hiera('nagios::contact_groups'),
  }
}
