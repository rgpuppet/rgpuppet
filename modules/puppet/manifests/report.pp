class puppet::report {
  package { 'librrd-ruby':
    ensure => installed,
  }

  file { '/var/www/puppet-rrd':
    ensure  => directory,
    mode    => '0755',
    owner   => 'puppet',
    group   => 'puppet',
    require => Package['apache2'],
  }

  file { "/var/lib/puppet/reports":
    owner        => 'puppet',
    group        => 'puppet',
    recurse      => true,
    recurselimit => 1,
    require      => Package[$puppet::master::pkg],
  }

  cron { 'expire-puppet-reports':
    command => '/usr/bin/find /var/lib/puppet/reports -mtime +8 -type f -exec /bin/rm "{}" ";"',
    user    => 'root',
    target  => 'root',
    hour    => '*',
    minute  => '7',
   }
}
