class puppet::category_report {
  $reportgen = '/usr/local/bin/category_report'

  file { $reportgen:
    content => template('puppet/category.py.erb'),
    mode    => '0755',
  }

  file { '/var/www/html/nodes.html':
    owner => 'puppet',
    mode  => '0644',
  }

  cron { 'category_report':
    command => $reportgen,
    user    => 'puppet',
    minute  => [2, 12, 22, 32, 42, 52],
  }
}
