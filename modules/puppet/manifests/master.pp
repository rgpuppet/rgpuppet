class puppet::master (
  $puppetmaster = hiera('puppet::master::puppetmaster'),
  $dbserver = hiera('puppet::master::dbserver'),
  $nscaservers = hiera('puppet::master::nscaservers')) {

  $pkg = 'puppetmaster-passenger'
  $srv = 'apache2'

  include puppet::hiera
  include puppet::report
  include puppet::category_report
  include recode

  $puppet_reports_to_nagios = hiera('puppet_reports_to_nagios')

  if $puppet_reports_to_nagios {
    include nagios::send_nsca

    file { '/etc/puppet/nagiosupdate_nsca.yaml':
      content => template('puppet/nagiosupdate_nsca.yaml.erb'),
      notify  => Service[$srv],
    }
  }
  else {
    file { '/etc/puppet/nagiosupdate_nsca.yaml':
      ensure => absent,
      notify => Service[$srv],
    }
  }

  # We run puppetmaster under apache, so tell the init script to not
  # start the standalone master.
  exec { '/bin/sed -i "s/^START=.*/START=no/" /etc/default/puppetmaster':
    unless => '/bin/grep -q "^START=no" /etc/default/puppetmaster',
  }

  class { "puppet::impl::client":
    is_master    => true,
    puppetmaster => $puppetmaster,
  }

  package { $pkg:
    ensure  => latest,
    require => Exec['update-apt'],
    notify  => Service[$srv],
  }

  # To get stored configs, we need rails.
  package { "rails":
    ensure => installed,
  }

  # To store them in PuppetDB we need this:
  package { ["puppetdb", "puppetdb-terminus"]:
    ensure => latest,
  }

  exec { "fix-default-puppetdb":
    command => "/bin/sed -i 's/-Xmx[0-9]*./-Xmx1g/' /etc/default/puppetdb",
    unless  => "/bin/grep -q '^[^#]*-Xmx1g' /etc/default/puppetdb",
    require => Package["puppetdb"],
    notify  => Service["puppetdb"],
  }

  exec { "/usr/sbin/puppetdb ssl-setup":
    require => Package["puppetdb"],
    notify  => Service["puppetdb"],
    creates => "/etc/puppetdb/ssl/private.pem",
  }

  service { "puppetdb":
    enable => true,
    ensure => running,
  }

  file { "/etc/puppet/puppetdb.conf":
    content => template("puppet/puppetdb.conf.erb"),
    require => Package["puppetdb"],
    notify  => Service[$srv],
  }

  file { "/etc/puppet/routes.yaml":
    content => template("puppet/routes.yaml.erb"),
    require => Package['puppetdb', $pkg],
    notify  => Service[$srv],
  }

  $master = '/var/git/rgpuppet.git'
  $user   = 'git'

  file { "/var/git":
    ensure => directory,
    owner  => $user,
    group  => $user,
    mode  =>  '0755',
  }

  exec { "/usr/bin/git init --bare ${master}":
    creates => $master,
    user    => $user,
    group   => $user,
    before  => File["${master}/hooks/post-receive"],
  }

  file { "${master}/hooks/post-receive":
    content => template("puppet/post-receive.erb"),
    mode    => '0555',
  }

  file { "/etc/puppet/environs":
    ensure => directory,
    owner  => $user,
    group  => $user,
    mode  =>  '0755',
  }
}
