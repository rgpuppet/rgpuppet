class puppet::cachedir {
  $cachedir = '/var/cache/puppet'

  file { $cachedir:
    ensure => directory,
  }
}
