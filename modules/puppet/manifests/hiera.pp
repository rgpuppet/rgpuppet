class puppet::hiera {
  package { "hiera":
    ensure => latest,
  }

  file { "/etc/puppet/hiera.yaml":
    content => template('puppet/hiera.yaml.erb'),
  }
}
