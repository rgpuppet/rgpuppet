class puppet::apt {
  include lsb_release
  include apt::update
  include puppet::cachedir

  $debfile = "${puppet::cachedir::cachedir}/puppetlabs-release.deb"
  $url = "http://apt.puppetlabs.com/puppetlabs-release-${lsbdistcodename}.deb"
  $aptlist = '/etc/apt/sources.list.d/puppetlabs.list'

  # The Ubuntu upgrade process will comment out all 3rd-party sources.
  # Remove the file if it only contains commented-out sources, so that
  # we can fetch a new one.
  exec { 'remove-commented-out-aptlist':
    command => "/bin/rm -f ${aptlist}",
    notify  => Exec['fetch-puppet-repo'],
    unless  => "/bin/grep -q '^deb' ${aptlist}",
  }

  exec { "fetch-puppet-repo":
    command => "/usr/bin/wget -O ${debfile} ${url}",
    creates => "${debfile}",
    require => [File[$puppet::cachedir::cachedir], Package['lsb-release']],

    # If the lsb-release package was not installed when facter
    # started, the lsbdistcodename expands to the empty string, so url
    # will be broken.  Unfortunately, facter isn't re-run when we
    # install lsb-release, so we have to wait until the next run
    # before we install the puppet repo.
    onlyif  => "/usr/bin/test '${lsbdistcodename}' != ''",
  }

  exec { "install-puppet-repo":
    command     => "/usr/bin/dpkg --force-confmiss -i ${debfile}",
    subscribe   => Exec["fetch-puppet-repo"],
    notify      => Exec['update-apt'],
    refreshonly => true,
  }

  # If wget failed to fetch a repo, but produced an empty output file,
  # remove it.  This won't fix things on this puppet run, but it will
  # set things up so that the next run might succeed.
  exec { 'rm-broken-puppet-repo':
    command => "/bin/rm ${debfile}",
    onlyif  => "/usr/bin/test -f ${debfile} && ! /usr/bin/test -s ${debfile}",
  }
}
