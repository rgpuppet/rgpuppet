class thunderbird {
  package { ["thunderbird", "thunderbird-gnome-support"]:
    ensure => installed,
  }
}

class thunderbird::sv {
  package { "thunderbird-locale-sv-se":
    ensure => installed,
  }
}
