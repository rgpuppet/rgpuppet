class automake::v1_7 {
  package { 'automake1.7':
    ensure => installed,
  }
}
