define debmirror($arch, $section, $release, $server, $root, $protocol) {

  include cron
  include apache

  $basedir = "$title"
  $script = "$basedir/domirror"
  $keyring = "$title/keyring/trustedkeys.gpg"
  $src_keyring = "/usr/share/keyrings/ubuntu-archive-keyring.gpg"
      
  file { ["$title", "$title/mirror", "$title/keyring"]:
    ensure => directory,
  }

  exec { "gpg --no-default-keyring --keyring $keyring --import $src_keyring":
    creates   => "$keyring",
    path      => "/usr/bin:/bin",
    require => File["$title/keyring"],
  }

  package { ["debmirror", "patch", "ed"]:
    ensure => installed,
  }

  file { "$script":
    content => template("debmirror/mirrorscript.erb"),
    mode => '0755',
  }  
  
  file { "/var/www/ubuntu":
    ensure  => "$basedir/mirror",
    require => Package["apache2"],
  }
}
