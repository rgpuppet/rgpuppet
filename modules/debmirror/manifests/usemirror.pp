define debmirror::usemirror($eolrelease) {
  $fallbackmirror = $title

  file { "/etc/apt/sources.list":
    content => template("debmirror/sources.list.erb"),
    notify  => Exec["apt_get_update_from_sources_list"],
    tag     => "bootstrap",
  }

  exec { "apt_get_update_from_sources_list":
    command     => "/usr/bin/apt-get update",
    refreshonly => true,
    tag         => "bootstrap",
  }

  Package {
    require => Exec["apt_get_update_from_sources_list"],
  }
}
