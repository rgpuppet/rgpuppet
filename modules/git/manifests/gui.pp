class git::gui {
  package { ['gitk', 'git-gui']:
    ensure => installed,
  }
}
