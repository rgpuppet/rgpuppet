class tftpd {
  package { "atftpd":
    ensure => installed,
  }

  file { "/tftpboot":
    ensure => directory,
    mode => '1777',
    owner => root,
  }

  service { "openbsd-inetd":
    enable  => true,
    require => Package["atftpd"],
  }    
}
