define dnseq::check($expected) {
  $domain = $title

  include dnseq

  nagios_service { "dnseq $domain":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "dnseq $domain",
    check_command       => "check_dnseq!$domain!$expected",
    target              => rg_nagios_target("dnseq-$domain"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}
