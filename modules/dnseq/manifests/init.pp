class dnseq {
  $checker = '/usr/lib/nagios/plugins/check_dnseq'

  file { $checker:
    content => template('dnseq/check_dnseq.sh.erb'),
    mode    => '0555',
  }

  nagios_command { "check_dnseq":
    command_line => "\$USER1\$/check_dnseq \$ARG1\$ \$ARG2\$",
    notify       => Service["nagios3"],
  }
}
