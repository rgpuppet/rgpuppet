class php5 {
  package { [ "php5", "php5-cgi", "php5-cli" ]:
    ensure => installed,
  }
}

class php5::mysql {
  package { [ "php5-mysql" ]:
    ensure => installed,
  }
}

