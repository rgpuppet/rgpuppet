define email::nullclient($relay, $myhostname, $mynetworks, $inet_interfaces, $rate_delay) {
  include postfix

  file { "/etc/postfix/main.cf":
    content => template("email/nullclient.erb"),
    notify  => Service["postfix"],
    require => Package["postfix"],
  }

  file { "/etc/postfix/master.cf":
    content => template("email/nullclient-master.erb"),
    notify  => Service["postfix"],
    require => Package["postfix"],
  }
}
