class fsecure {
  # Manually download .deb files from 
  # http://www.f-secure.com/en_EMEA/downloads/product-updates/policy-manager-for-linux/
  # to antivirus:/deb/*.deb.

  # Manually run 
  #
  #     /opt/f-secure/fspms/bin/fspms-config
  #
  # to complete the configuration.

  include cron
  include libstdcxx5

  package { "f-secure-automatic-update-agent":
    provider => dpkg,
    ensure   => installed,
    source   => "/deb/f-secure-automatic-update-agent_8.24.2976_i386.deb",
  }

  package { "f-secure-policy-manager-console":
    provider => dpkg,
    ensure   => installed,
    source   => "/deb/f-secure-policy-manager-console_8.10.25405_i386.deb",
  }

  package { "f-secure-policy-manager-server":
    provider => dpkg,
    ensure   => installed,
    source   => "/deb/f-secure-policy-manager-server_8.10.25405_i386.deb",
    require  => [Package["f-secure-automatic-update-agent"],
                 Package["libstdc++5"]],
  }

  package { "f-secure-policy-manager-web-reporting":
    provider => dpkg,
    ensure   => installed,
    source   => "/deb/f-secure-policy-manager-web-reporting_8.10.25405_i386.deb",
    require  => Package["f-secure-policy-manager-server"],
  }
}
