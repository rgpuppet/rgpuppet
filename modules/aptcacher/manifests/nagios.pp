class aptcacher::nagios {
  nagios_command { 'check-aptcacher-ng':
    command_line => '/usr/lib/nagios/plugins/check_http -I $HOSTADDRESS$ -p 3142 -e "404 OK"',
  }
}
