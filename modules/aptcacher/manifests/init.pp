class aptcacher($adminuser = hiera('aptcacher::adminuser'),
                $adminpassword = hiera('aptcacher::adminpassword')) {
  package { "apt-cacher-ng":
    ensure => installed,
  }


  $security = '/etc/apt-cacher-ng/security.conf'

  exec { 'set-apt-cacher-ng-password':
    command => "/bin/sed -i 's/^#*AdminAuth.*/AdminAuth: ${adminuser}:${adminpassword}/' ${security}",
    unless  => "/bin/grep '^AdminAuth: ${adminuser}:${adminpassword}\$' ${security}",
    require => Package['apt-cacher-ng'],
    notify  => Service['apt-cacher-ng'],
  }

  service { 'apt-cacher-ng':
    enable => true,
    ensure => running,
  }

  @@nagios_service { "$hostname apt-cache":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => 'apt-cache',
    check_command       => 'check-aptcacher-ng',
    target              => rg_nagios_target('apt-cache'),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}
