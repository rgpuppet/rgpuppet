class vc::subversion {
  package { 'subversion':
    ensure => installed,
  }
}
