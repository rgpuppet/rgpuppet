class vc::mercurial {
  package { 'mercurial':
    ensure => installed,
  }
}
