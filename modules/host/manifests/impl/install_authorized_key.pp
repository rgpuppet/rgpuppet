define host::impl::install_authorized_key($user, $keyname) {
  $sshkeys = hiera('sshkeys')
  $key = $sshkeys[$keyname]

  ssh_authorized_key { "${user}-${keyname}":
    ensure => present,
    key    => $key['key'],
    type   => $key['type'],
    user   => $user,
  }
}
