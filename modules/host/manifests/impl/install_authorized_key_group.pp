define host::impl::install_authorized_key_group($user, $group) {

  $sshgroups = hiera('sshgroups')

  $keys = $sshgroups[$group]['keys']
  if $keys {
    $keys.each |$keyname| {
      host::impl::install_authorized_key { "${user}-${keyname}":
        user    => $user,
        keyname => $keyname,
      }
    }
  }

  $groups = $sshgroups[$group]['groups']
  if $groups {
    $groups.each |$subgroup| {
      host::impl::install_authorized_key_group { "${user}-${subgroup}":
        user  => $user,
        group => $subgroup,
      }
    }
  }
}
