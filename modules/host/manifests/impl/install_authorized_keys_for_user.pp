define host::impl::install_authorized_keys_for_user {

  $user_group = $title

  $user  = regsubst($user_group, '([^:]*):.*', '\1')
  $group = regsubst($user_group, '[^:]*:(.*)', '\1')

  host::impl::install_authorized_key_group { "${user}-${group}":
    user  => $user,
    group => $group,
  }
}
