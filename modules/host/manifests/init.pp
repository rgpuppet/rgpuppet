class host(
  $legend,
  $hostgroup,
  $parent = undef,
  $switch = undef,
  $switch_port = undef,
  $category = undef,
  $enclosure = undef,
  $ip_override = undef,
  $authorized_keys = hiera('host::authorized_keys'),
  $hostgroup_authorized_keys = hiera('host::hostgroup_authorized_keys'),
  ) {

  include nagios::check_fsck

  if $authorized_keys {
    host::impl::install_authorized_keys_for_user { $authorized_keys: }
  }

  $ak = $hostgroup_authorized_keys[$hostgroup]
  if $ak {
    host::impl::install_authorized_keys_for_user { $ak: }
  }

  include timezone

  $arrayified_category = *$category

  if $virtual == "kvm" and ! member($arrayified_category, 'offsite') {
    kvm::parent { "${parent}": }
    $kvm_parent = $parent
    $kvm_host = false
  }
  elsif member($arrayified_category, 'kvm') {
    $kvm_parent = undef
    $kvm_host = true
  }
  else {
    $kvm_parent = undef
    $kvm_host = false
  }

  class { 'nagios::host':
    legend      => $legend,
    hostgroup   => $hostgroup,
    parent      => $parent,
    switch      => $switch,
    switch_port => $switch_port,
    kvm_parent  => $kvm_parent,
    kvm_host    => $kvm_host,
    ip_override => $ip_override,
  }

  if $category {
    host::category { $category: }
  }

  if $enclosure {
    host::enclosure { $enclosure: }
  }

  if $kvm_parent {
    $verbose_legend = "${legend} (${virtual} @ ${parent})\n"
  }
  elsif $enclosure {
    $verbose_legend = "${legend} (enclosure ${enclosure})\n"
  }
  else {
    $verbose_legend = "${legend}\n"
  }

  file { '/etc/legend':
    content => $verbose_legend,
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
  }
}
