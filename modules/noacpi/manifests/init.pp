class noacpi {
  case $lsbdistcodename {
    'hardy','intrepid': {
      $file = "/boot/grub/menu.lst"
      
      exec { "/bin/sed -i.bak 's/^\\(# kopt=.*\\)/\\1 pci=noacpi/' $file":
        unless => "/bin/grep -q '^# kopt=.*pci=noacpi' $file",
        notify => Exec["/usr/sbin/update-grub"],
        tag    => "bootstrap",
      }
    }
    default: {
      $file = "/etc/default/grub"
    
      exec { "/bin/sed -i.bak 's/^\\(GRUB_CMDLINE_LINUX_DEFAULT=\".*\\)\"/\\1 pci=noacpi\"/' $file":
        unless => "/bin/grep -q '^GRUB_CMDLINE_LINUX_DEFAULT=.*pci=noacpi' $file",
        notify => Exec["/usr/sbin/update-grub"],
        tag    => "bootstrap",
      }
    }
  }
  exec { "/usr/sbin/update-grub":
    refreshonly => true,
    tag         => "bootstrap",
  }
}
