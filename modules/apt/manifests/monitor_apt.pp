class apt::monitor_apt {
  if versioncmp($lsbdistrelease, '10.04') >= 0 {
    $can_monitor = true
    $apt_check = "/usr/lib/update-notifier/apt-check"
  }
  elsif $lsbdistrelease == '8.04' or $lsbdistrelease == '8.10' {
    $can_monitor = true
    $apt_check = "/usr/local/bin/apt-check-for-8.04"

    file { "$apt_check":
      content => template("apt/apt-check-for-8.04.erb"),
      mode    => '0755',
    }

    package { "python-apt":
      ensure => installed,
    }
  }
  else {
    $can_monitor = false
  }

  if $can_monitor {
    include nagios::nrpe

    package { 'update-notifier-common':
      ensure => installed,
    }

    file { '/usr/lib/nagios/plugins/check_apt':
      content => template('apt/check_apt.erb'),
      notify  => Service['nagios-nrpe-server'],
      mode    => '0755',
    }

    file { '/etc/nagios/nrpe.d/check_apt.cfg':
      content => "command[check_apt]=/usr/lib/nagios/plugins/check_apt\n",
      notify  => Service['nagios-nrpe-server'],
    }

    @@nagios_service { "$hostname check-apt":
      use                 => 'rg-service',
      host_name           => $hostname,
      service_description => 'apt',
      servicegroups       => 'apt',
      check_command       => 'check-nrpe-timeout!90!check_apt',
      check_interval      => '10',
      stalking_options    => 'c',
      target              => rg_nagios_target('apt'),
      contact_groups      => hiera('nagios::contact_groups'),
      icon_image          => rg_graphios(
        'apt.{security,total}',
        '&areaMode=all&colorList=red,orange&yMin=0',
        '&bgcolor=black',
      ),
    }
  }
}
