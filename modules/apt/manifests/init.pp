class apt {
  include apt::monitor_apt

  exec { 'periodic_apt_get_update':
    command  => '/usr/bin/apt-get update',
    schedule => 'daily',
  }
}
