class apt::update {
  exec { 'update-apt':
    command     => '/usr/bin/apt-get update',
    refreshonly => true,
  }
}
