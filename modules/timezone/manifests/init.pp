class timezone($timezone = hiera('timezone::timezone')) {

  file { '/etc/timezone':
    content => "${timezone}\n",
  }

  exec { '/usr/sbin/dpkg-reconfigure -fnoninteractive tzdata':
    subscribe   => File['/etc/timezone'],
    refreshonly => true,
  }
}
