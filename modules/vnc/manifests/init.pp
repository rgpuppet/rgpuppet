class vnc::server {
  package { "vnc4server":
    ensure => installed,
  }
}

class vnc::viewer {
  package { "xvnc4viewer":
    ensure => installed,
  }
}
