class gnome_user_guide_sv {
  if versioncmp($lsbdistrelease, "10.04") >= 0 {
    package { "gnome-user-guide-sv":
      ensure => installed,
    }
  }
}
