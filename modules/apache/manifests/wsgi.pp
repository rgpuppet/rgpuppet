class apache::wsgi {
  include apache

  package { 'libapache2-mod-wsgi-py3':
    ensure => installed,
  }
}
