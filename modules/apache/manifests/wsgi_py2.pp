class apache::wsgi_py2 {
  include apache

  package { 'libapache2-mod-wsgi':
    ensure => installed,
  }
}
