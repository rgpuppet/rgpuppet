class apache {
  service { "apache2":
    enable  => true,
    require => Package["apache2"],
  }

  package { ['apache2', 'apache2-doc', 'apache2-utils']:
    ensure => installed,
  }

  @@nagios_service { "$hostname http":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "http",
    check_command       => "check_httpname",
    target              => rg_nagios_target("http"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}

class apache::ssl {
  exec { "/usr/sbin/a2enmod ssl":
    unless => "/usr/bin/test -f /etc/apache2/mods-enabled/ssl.load",
    notify => Service["apache2"],
  }

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    $suffix = ''
  }
  else {
    $suffix = '.conf'
  }

  exec { "/usr/sbin/a2ensite default-ssl":
    unless => "/usr/bin/test -f /etc/apache2/sites-enabled/default-ssl${suffix}",
    notify => Service["apache2"],
  }
  include ssl_cert

  @@nagios_service { "$hostname https":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "https",
    check_command       => "check-https!443",
    target              => rg_nagios_target("https"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}

define apache::certificate($cert, $chain) {
  $key = $title

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    $suffix = ''
  }
  else {
    $suffix = '.conf'
  }

  $cfg = "/etc/apache2/sites-available/default-ssl${suffix}"

  exec { "/bin/sed -i 's%^\\([ \t]*\\)\\(#[ \t]*\\)\\(SSLCertificateFile[ \t]*\\).*%\\1\\3$cert%' $cfg":
    unless => "/bin/grep -q '^[ \t]*SSLCertificateFile[ \t]*$cert' $cfg",
    notify => Service["apache2"],
  }

  exec { "/bin/sed -i 's%^\\([ \t]*\\)\\(#[ \t]*\\)*\\(SSLCertificateKeyFile[ \t]*\\).*%\\1\\3$key%' $cfg":
    unless => "/bin/grep -q '^[ \t]*SSLCertificateKeyFile[ \t]*$key' $cfg",
    notify => Service["apache2"],
  }

  exec { "/bin/sed -i 's%^\\([ \t]*\\)\\(#[ \t]*\\)\\(SSLCertificateChainFile[ \t]*\\).*%\\1\\3$chain%' $cfg":
    unless => "/bin/grep -q '^[ \t]*SSLCertificateChainFile[ \t]*$chain' $cfg",
    notify => Service["apache2"],
  }
}

class apache::nagios {
  nagios_command { "check-https":
    command_line => "/usr/lib/nagios/plugins/check_http --ssl -p \$ARG1\$ -H '\$HOSTNAME\$' -I '\$HOSTADDRESS\$' -C 45",
    notify       => Service["nagios3"],
  }
}
