class ubuntu_supported {
  include launchpadlib
  include puppetdb_python
  include nagios::nrpe

  $checker = "/usr/lib/nagios/plugins/ubuntu_supported"

  file { "$checker":
    content => template("ubuntu_supported/ubuntu_supported.erb"),
    mode    => '0755',
  }

  file { "/etc/nagios/nrpe.d/check_ubuntu_supported.cfg":
    content => "command[check_ubuntu_supported]=$checker\n",
    notify  => Service["nagios-nrpe-server"],
  }

  @@nagios_service { "$hostname check-ubuntu-supported":
    use                  => "nrpe-service",
    host_name            => "$hostname",
    service_description  => "Ubuntu version",
    check_command        => "check_nrpe_1arg!check_ubuntu_supported",
    check_interval       => 150,
    retry_check_interval => 15,
    target               => rg_nagios_target("ubuntu_supported"),
    contact_groups       => hiera('nagios::contact_groups'),
  }
}
