class rt {
  include mysql::server

  package { ["rt3.8-db-mysql", "request-tracker3.8"]:
    ensure  => installed,
    require => Package["mysql-server"],
  }

  exec { "update-rt-siteconfig":
    command     => "/usr/sbin/update-rt-siteconfig-3.8",
    refreshonly => true,
    require     => Package["request-tracker3.8"],
  }
}
