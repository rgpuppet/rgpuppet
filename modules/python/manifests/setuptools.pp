class python::setuptools {
  package { 'python-setuptools':
    ensure => installed,
  }
}
