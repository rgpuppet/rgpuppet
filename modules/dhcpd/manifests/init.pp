class dhcpd($master, $slave, $config,
            $dns_servers=hiera('dns::servers'),
            $ntp_servers=hiera('ntp::servers'),
            $omapi_key=hiera('dhcpd::omapi_key'),
            $omapi_port=hiera('dhcpd::omapi_port'),
) {
  package { "isc-dhcp-server":
    ensure => installed,
  }

  file { '/etc/dhcp/dhcpd.conf':
    content => template("dhcpd/dhcpd.conf.erb"),
    require => Package['isc-dhcp-server'],
    notify  => Service['isc-dhcp-server'],
  }

  service { 'isc-dhcp-server':
    enable => true,
    ensure => running,
  }
}
