# Stuff needed on the Nagios server to monitor the DHCP servers.

class dhcpd::monitor($master=hiera('dhcpd::master'),
                     $slave=hiera('dhcpd::slave'),
                     $omapi_key=hiera('dhcpd::omapi_key'),
                     $omapi_port=hiera('dhcpd::omapi_port'),
) {
  # Part one: check that we get a response from all DHCP servers.

  file { '/etc/sudoers.d/check_dhcp':
    content => template('dhcpd/sudo_check_dhcp.erb'),
    mode    => '0440',
    owner   => 'root',
    group   => 'root',
  }

  nagios_service { 'dhcp':
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => 'DHCP service',
    servicegroups       => 'dhcp',
    check_command       => 'check-dhcp',
    target              => rg_nagios_target('dhcp'),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  nagios_command { 'check-dhcp':
    command_line => "/usr/bin/sudo \$USER1\$/check_dhcp -i eth0 -s ${master} -s ${slave} -r ${::ipaddress}",
    notify       => Service["nagios3"],
  }

  # Part two: check that the pools are not exhausted.

  include pypureomapi

  $dhcp_servers = [$master, $slave]

  file { '/usr/lib/nagios/plugins/check_dhcpd_leases':
    content => template('dhcpd/check_dhcpd_leases.py.erb'),
    mode    => '0755',
  }

  nagios_command { 'check-dhcpd-lease':
    command_line => '/usr/lib/nagios/plugins/check_dhcpd_leases $ARG1$',
  }

  nagios_servicegroup { 'dhcp':
    alias => 'dhcp'
  }

  dhcpd::monitor::net { '6':
    short => 'audio',
  }

  dhcpd::monitor::net { '10':
    short => 'office',
  }

  dhcpd::monitor::net { '11':
    short => 'surveillance',
  }

  dhcpd::monitor::net { '8':
    short => 'guest',
  }

  dhcpd::monitor::net { '9':
    short => 'conference',
  }
}

define dhcpd::monitor::net($short) {
  $subnet = $title

  nagios_service { "dhcp leases $short":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => "leases $short",
    servicegroups       => 'dhcp',
    check_command       => "check-dhcpd-lease!192.168.${subnet}",
    target              => rg_nagios_target("leases"),
    contact_groups      => hiera('nagios::contact_groups'),
  }
}
