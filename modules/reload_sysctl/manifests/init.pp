class reload_sysctl {
  exec { 'reload-sysctl':
    command     => '/sbin/sysctl --system',
    refreshonly => true,
  }
}
