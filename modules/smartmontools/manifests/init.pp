class smartmontools {
  package { "smartmontools":
    ensure => installed,
  }
}

define smartmontools::nagios($interface="ata") {
  include smartmontools::check_smart

  $disk = $title

  @@nagios_service { "$hostname smart $disk":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "$disk SMART",
    check_command       => "check_nrpe_1arg!check_smart_$disk",
    target              => rg_nagios_target("smart"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  file { "/etc/nagios/nrpe.d/check_smart_$disk.cfg":
    content => "command[check_smart_$disk]=/usr/lib/nagios/plugins/check_smart --device=/dev/$disk --interface=$interface\n",
    notify  => Service["nagios-nrpe-server"],
  }
}

class smartmontools::check_smart {
  file { "/usr/lib/nagios/plugins/check_smart":
    content => template("smartmontools/check_smart.erb"),
    mode    => '0755',
  }

  exec { "/bin/echo 'nagios ALL = NOPASSWD: /usr/sbin/smartctl' >> /etc/sudoers":
    unless => "/bin/grep -q smartctl /etc/sudoers",
  }
}
