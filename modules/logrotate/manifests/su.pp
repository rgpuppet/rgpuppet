class logrotate::su {
  $cfg = "/etc/logrotate.conf"

  exec { "/bin/sed -i '1isu root syslog' ${cfg}":
    unless => "/bin/grep -q '^su' ${cfg}",
    onlyif => "/usr/bin/test -f ${cfg}",
  }
}
