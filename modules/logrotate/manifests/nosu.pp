class logrotate::nosu {
  $cfg = "/etc/logrotate.conf"

  exec { "/bin/sed -i '/^su root syslog$/d' ${cfg}":
    onlyif => "/bin/grep -q '^su' ${cfg}",
  }
}
