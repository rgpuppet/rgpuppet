class logrotate {
  package { "logrotate":
    ensure => installed,
  }

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    include logrotate::nosu
  }
  else {
    include logrotate::su
  }
}

class logrotate::keep_wtmp {
  $cfg = "/etc/logrotate.conf"
  exec { "/bin/sed -i '/^\\/var\\/log\\/wtmp/,/^}/s/^/#/' $cfg":
    unless => "/bin/grep -q '^#/var/log/wtmp' $cfg",
    onlyif => "/usr/bin/test -f $cfg",
  }
}
