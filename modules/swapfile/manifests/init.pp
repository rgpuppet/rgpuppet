class swapfile($megabyte) {
  exec { "/bin/dd if=/dev/zero of=/swapfile bs=1048576 count=${megabyte}":
    creates => "/swapfile",
    notify  => Exec['/sbin/mkswap /swapfile'],
  }

  exec { '/sbin/mkswap /swapfile':
    refreshonly => true,
    notify      => Exec['/sbin/swapon -a'],
  }

  exec { '/bin/echo /swapfile none swap sw 0 0 >> /etc/fstab':
    unless => '/bin/grep -q "^/swapfile none swap" /etc/fstab',
    notify => Exec['/sbin/swapon -a'],
  }

  exec { '/sbin/swapon -a':
    refreshonly => true,
  }
}
