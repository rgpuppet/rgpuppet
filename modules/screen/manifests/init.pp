class screen {
  package { "screen":
    ensure => installed,
  }

  # Screen wants mode 775 nowadays.
  file { '/var/run/screen':
    ensure => directory,
    mode   => '0775',
  }
}
