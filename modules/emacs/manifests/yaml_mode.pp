class emacs::yaml_mode {
  package { "yaml-mode":
    ensure => installed,
  }
}
