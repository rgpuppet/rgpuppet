class emacs {
  $emacsversion = $operatingsystem ? {
    "Solaris" => "22",
    "Ubuntu"  => $lsbdistrelease ? {
      "10.04" => "22",
      "10.10" => "23",
      "11.04" => "23",
      "11.10" => "23",
      "12.04" => "23",
      "13.04" => "24",
      "13.10" => "24",
      "14.04" => "24",
      "16.04" => "24",
      default => "22",
    },
    "Debian"  => "23",
    default   => "",
  }

  $emacssuffix = $operatingsystem ? {
    "Ubuntu" => $lsbdistrelease ? {
      "10.04" => "-gtk",
      "10.10" => "-gtk",
      default => "",
    },
    default  => "",
  }

  $emacs = "emacs$emacsversion$emacssuffix"

  $emacsel = "emacs$emacsversion-el"

  package { "$emacs":
    ensure => installed,
  }

  package { "$emacsel":
    ensure => installed,
  }
}
