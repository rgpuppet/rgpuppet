class emacs::exuberant_ctags {
  package { "exuberant-ctags":
    ensure => installed,
  }
}
