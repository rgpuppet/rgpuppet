# Dependencies needed to build Emacs from source.
class emacs::build {
  include libxpm
  include libgif
  include libtiff
}
