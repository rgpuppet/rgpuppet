class denyhosts {
  package { "denyhosts":
    ensure => installed,
  }

  service { "denyhosts":
    enable => true,
  }
}
