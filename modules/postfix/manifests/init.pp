class postfix {
  package { "postfix":
    ensure => installed,
  }

  service { "postfix":
    enable => true,
  }
}
