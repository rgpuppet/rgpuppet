class unbound($stub_zones,
              $interface_prefixes = hiera('interface_prefixes'),
              $dns_servers = hiera('dns::servers')) {
  package { 'unbound':
    ensure => installed,
  }

  file { '/etc/unbound/unbound.conf':
    require => Package['unbound'],
    notify  => Service['unbound'],
    content => template('unbound/unbound.conf.erb'),
  }

  service { 'unbound':
    ensure    => 'running',
    enable    => true,
    hasstatus => false,
  }
}
