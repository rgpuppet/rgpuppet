class bind {
  include dnsutils

  package { ["bind9", "bind9-doc"] :
    ensure => installed,
  }

  service { "bind9":
    enable  => true,
    pattern => "/usr/sbin/named",
  }
}
