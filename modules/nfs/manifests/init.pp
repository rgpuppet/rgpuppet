class nfs::client {
  package { "nfs-common":
    ensure => installed,
  }
}

define nfs::server($exports) {
  package { "nfs-kernel-server":
    ensure => installed,
  }

  exec { "exportfs":
    command      => "/usr/sbin/exportfs -a",
    refreshonly  => true,
  }

  file { "/etc/exports":
    content => template("nfs/exports.erb"),
    notify  => Exec["exportfs"],
    require => Package["nfs-kernel-server"],
  }
}
