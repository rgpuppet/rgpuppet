class ftpd {
  package { "vsftpd":
    ensure => installed,
  }

  service { "vsftpd":
    enable => true,
  }

  $cfg = "/etc/vsftpd.conf"

  exec { "/bin/sed -i 's/^#\\?\\(anonymous_enable\\)=.*/\\1=NO/' $cfg":
    unless  => "/bin/grep -q '^anonymous_enable=NO\$' $cfg",
    require => Package["vsftpd"],
    notify  => Service["vsftpd"],
  }

  exec { "/bin/sed -i 's/^#\\(local_enable=YES\\)\$/\\1/' $cfg":
    unless  => "/bin/grep -q '^local_enable=YES\$' $cfg",
    require => Package["vsftpd"],
    notify  => Service["vsftpd"],
  }

  exec { "/bin/sed -i 's/^#\\(write_enable=YES\\)\$/\\1/' $cfg":
    unless  => "/bin/grep -q '^write_enable=YES\$' $cfg",
    require => Package["vsftpd"],
    notify  => Service["vsftpd"],
  }

  exec { "/bin/echo 'syslog_enable=YES' >> $cfg":
    unless  => "/bin/grep -q '^syslog_enable=YES\$' $cfg",
    require => Package["vsftpd"],
    notify  => Service["vsftpd"],
  }

  @@nagios_service { "$hostname ftp":
    use                 => "rg-service",
    host_name           => "$hostname",
    service_description => "FTP",
    check_command       => "check_ftp",
    target              => rg_nagios_target("ftp"),
    contact_groups      => hiera('nagios::contact_groups'),
  }

  include ftpd::fix_ftp_group
}

class ftpd::fix_ftp_group {
  exec { "/usr/bin/ypmatch ftp group >> /etc/group":
    onlyif => "/usr/bin/test -f /usr/bin/ypmatch && /usr/bin/ypmatch ftp group>/dev/null",
    unless => "/bin/grep '^ftp:' /etc/group",
  }
}
