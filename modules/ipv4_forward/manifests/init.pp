class ipv4_forward {
  include reload_sysctl

  file { '/etc/sysctl.d/60-ipv4-forward.conf':
    content => "net.ipv4.ip_forward=1\n",
    notify  => Exec['reload-sysctl'],
  }
}
