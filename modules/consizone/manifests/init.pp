class consizone($dns_servers = hiera('dns::servers'),
                $dns_resolvers = hiera('dns::resolvers'),
                $zone = hiera('dns::faketld'))
{
  include dnspython

  $checker = '/usr/lib/nagios/plugins/check_consizone'

  file { $checker:
    content => template('consizone/consizone.py.erb'),
    mode    => '0555',
  }

  nagios_command { 'check-consizone':
    command_line => "${checker}",
  }

  nagios_service { "${hostname} consizone":
    use                 => 'rg-service',
    host_name           => $hostname,
    service_description => 'DNS consistency',
    check_command       => 'check-consizone',
    target              => rg_nagios_target("consizone"),
    contact_groups      => hiera('nagios::contact_groups'),
    icon_image          => rg_graphios('issues'),
  }
}
