class pdf {
  if versioncmp($lsbdistrelease, "12.04") < 0 {
    $pkg = 'xpdf-reader'
  }
  else {
    $pkg = 'xpdf'
  }

  package { $pkg:
    ensure => installed,
  }
}
