class puppetdb_python {
  include pip

  exec { '/usr/bin/pip install puppetdb':
    unless => '/usr/bin/python -c "import puppetdb"',
  }
}
