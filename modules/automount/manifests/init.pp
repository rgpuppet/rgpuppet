class automount {
  package { "autofs":
    ensure => installed,
  }

  service { "autofs":
    enable  => true,
    require => Package["autofs"],
  }
}
