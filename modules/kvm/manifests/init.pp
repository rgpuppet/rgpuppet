class kvm {
  include vnc::server

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    package { 'kvm':
      ensure => installed,
    }
  }
  else {
    package { 'qemu-kvm':
      ensure => installed,
    }
  }

  package { ["virt-manager", "libvirt-bin"]:
    ensure => installed,
  }

  Exec <<| tag == "$hostname" |>>

  include nagios::nrpe

  $virt_mem = "/usr/lib/nagios/plugins/check_virt_mem"
  file { "$virt_mem":
    content => template("kvm/check_virt_mem.erb"),
    mode    => '0755',
  }

  file { "/etc/nagios/nrpe.d/check_virt_mem.cfg":
    content => "command[check_virt_mem]=$virt_mem\n",
    notify  => Service["nagios-nrpe-server"],
  }

  @@nagios_service { "$hostname check-virt-mem":
    use                 => "nrpe-service",
    host_name           => "$hostname",
    service_description => "virtmem",
    check_command       => "check_nrpe_1arg!check_virt_mem",
    target              => rg_nagios_target("virt-mem"),
    servicegroups       => "virtmem",
    contact_groups      => hiera('nagios::contact_groups'),
  }

  exec { "addgroup nagios libvirtd":
    command => "/usr/sbin/addgroup nagios libvirtd",
    unless  => "/bin/grep -q '^libvirtd:.*[:,]nagios\\(,\\|\$\\)' /etc/group",
    notify  => Service["nagios-nrpe-server"],
  }
}

define kvm::parent {
  @@exec { "/usr/bin/virsh autostart $hostname":
    unless => "/usr/bin/virsh dominfo $hostname | /bin/grep -q '^Autostart:.*enable'",
    tag    => $title,
  }
}

class kvm::nagios {
  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    file { "/etc/nagiosgrapher/ngraph.d/check_virt_mem.ncfg":
      content => template("kvm/check_virt_mem.ncfg.erb"),
    }

    file { "/etc/nagiosgrapher/ngraph.d/extra/check_hardware.ncfg":
      ensure => absent,
    }
  }

  nagios_servicegroup { "virtmem":
    alias => "virtmem",
  }
}
