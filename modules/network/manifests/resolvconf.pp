define network::resolvconf($servers, $extrasearch) {
  file { "/etc/resolv.conf":
    content => template("network/resolv.conf.erb"),
  }
}
