class network::common {
  service { "networking":
    enable     => true,
    hasrestart => false,
    tag        => "bootstrap",
  }

  if versioncmp($lsbdistrelease, "10.04") < 0 {
    service { "dhcdbd":
      enable => false,
      tag    => "bootstrap",
    }
  }

  file { "/etc/hostname":
    content => "$fqdn\n",
    owner   => root,
    group   => root,
    mode    => '0644',
    notify  => Service["networking"],
    tag     => "bootstrap",
  }
}

class network::fromdns($gateway = $gateway) {
  include network::common
  include ntpd::static_ip

  file { "/etc/network/interfaces":
    content => template("network/interfaces.fromdns.erb"),
    owner   => root,
    group   => root,
    mode    => '0644',
    notify  => Service["networking"],
    tag     => "bootstrap",
  }
}

class network::fromdns_dmz {
  class { 'network::fromdns':
    gateway => $dmz_gateway,
  }
}

class network::fromdns_dmz_2 {
  class { 'network::fromdns':
    gateway => $dmz_2_gateway,
  }
}

class network::bridgedfromdns {
  include network::common
  include bridge_utils
  include ntpd::static_ip

  file { "/etc/network/interfaces":
    content => template("network/interfaces.bridged.fromdns.erb"),
    owner   => root,
    group   => root,
    mode    => '0644',
    notify  => Service["networking"],
  }
}
