class network::multi($interface_prefixes = hiera('interface_prefixes')) {
  include network::common
  include ntpd::static_ip

  file { "/etc/network/interfaces":
    content => template("network/interfaces.multi.erb"),
    owner   => root,
    group   => root,
    mode    => '0644',
    notify  => Service["networking"],
    tag     => "bootstrap",
  }
}
