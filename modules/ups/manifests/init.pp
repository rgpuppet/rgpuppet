class ups::nagios {
  nagios_servicegroup { "ups":
    alias => "ups",
  }

  if versioncmp($::lsbdistrelease, '14.04') < 0 {
    file { "/etc/nagiosgrapher/ngraph.d/ups.ncfg":
      content => template("ups/ups.ncfg.erb"),
      notify  => Service["nagiosgrapher"],
    }
  }
}
